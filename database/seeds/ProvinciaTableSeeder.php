<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProvinciaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_provincia')->insert([
            [
                'id' => '1',
                'codigo' => 'V7',
                'nome' => 'Bengo',
            ],
            [
                'id' => '2',
                'codigo' => 'A3',
                'nome' => 'Benguela'
            ],
            [
                'id' => '3',
                'codigo' => 'L5',
                'nome' => 'Bie',
            ],
            [
                'id' => '4',
                'codigo' => 'D7',
                'nome' => 'Cabinda',
            ],
            [
                'id' => '5',
                'codigo' => 'N9',
                'nome' => 'Cunene',
            ],
            [
                'id' => '6',
                'codigo' => 'C9',
                'nome' => 'Cuanza Norte',
            ],
            [
                'id' => '7',
                'codigo' => 'U2',
                'nome' => 'Cuanda Cubango',
            ],
            [
                'id' => '8',
                'codigo' => 'H6',
                'nome' => 'Cuanza Sul',
            ],
            [
                'id' => '9',
                'codigo' => 'V1',
                'nome' => 'Huila',
                
            ],
            [
                'id' => '10',
                'codigo' => 'U3',
                'nome' => 'Huambo',
                
            ],
            [
                'id' => '11',
                'codigo' => 'T7',
                'nome' => 'Luanda',
            ],
            [
                'id' => '12',
                'codigo' => 'H8',
                'nome' => 'Lunda Sul',
            ],
            [
                'id' => '13',
                'codigo' => 'S2',
                'nome' => 'Lunda Norte',
            ],
            [
                'id' => '14',
                'codigo' => 'E4',
                'nome' => 'Malange',
            ],
            [
                'id' => '15',
                'codigo' => 'B6',
                'nome' => 'Moxico',
            ],
            [
                'id' => '16',
                'codigo' => 'B9',
                'nome' => 'Namibe',
            ],
            [
                'id' => '17',
                'codigo' => 'J9',
                'nome' => 'Uige',
            ],

            [
                'id' => '18',
                'codigo' => 'Q9',
                'nome' => 'Zaire',
            ],
        ]);
    }
}
 
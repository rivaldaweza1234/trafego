<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TipoEstacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      
        DB::table('tbl_estacao_trabalho')->insert([
       
            'municipio_id' => '1',
            'tipo' => 'Sede',
            'nome' => 'Luanda', 
            ]);
      
            //
        }
    }
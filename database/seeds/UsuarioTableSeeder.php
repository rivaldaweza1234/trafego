<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class UsuarioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_usuario')->insert([
      
            [
                'codigo' => '1000',
                'nome' => 'Admin',
                'password' => '$2y$12$CPuJ5zzQx6UTVz9g2jJibejCT9OUMF5.BgiCFRodnXJv7ZFJtn6A.',
                'id_estacao_trabalho' => '1',
            ],
            [
                'codigo' => '1001',
                'nome' => 'Rivaldo Antonio',
                'password' => '$2y$12$fD8X2S2cE4oBQF5fwSJkJO.6L4P5TYmQjZoh.JiLUA4P9/74U427u',
                'id_estacao_trabalho' => '1',
            ],
            
    ]);
      
        //
    }
}

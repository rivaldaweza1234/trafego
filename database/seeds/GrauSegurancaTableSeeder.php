<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GrauSegurancaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_grau_seguranca')->insert([
            [
                 
                'descricao' => 'Confidencial',
               
            ],
            [
                 
                'descricao' => 'Não Classificada',

            ],
            ]);
    }
}
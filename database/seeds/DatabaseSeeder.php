<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(GrauPrecedenciaTableSeeder::class);
        $this->call(OcorrenciaTableSeeder::class);
        $this->call(ProvinciaTableSeeder::class);
        $this->call(MunicipioTableSeeder::class);
        $this->call(TipoOperacaoTableSeeder::class);
        $this->call(TipoTrafegoTableSeeder::class);
        $this->call(EstacaoTableSeeder::class);
        $this->call(UsuarioTableSeeder::class);
        $this->call(GrauSegurancaTableSeeder::class);
       
    
       
    }
}

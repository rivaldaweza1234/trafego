<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class MunicipioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tbl_municipio')->insert([
            [
                'id' => '1',
                'provincia_id' => '1',
                'nome' => 'Ambriz',
            ],
            [
                'id' => '2',
                'provincia_id' => '1',
                'nome' => 'Bula Atumba',
                
            ],

            [
                'id' => '3',
                'provincia_id' => '1',
                'nome' => 'Dande',
                
            ],
            [
                'id' => '4',
                'provincia_id' => '1',
                'nome' => 'Dembos',
                
            ],
            [
                'id' => '5',
                'provincia_id' => '1',
                'nome' => 'Nambuangongo',
                
            ],
            [
                'id' => '6',
                'provincia_id' => '1',
                'nome' => 'Pango Aluquém',
                
            ],
            [
                'id' => '7',
                'provincia_id' => '2',
                'nome' => 'Balombo',
                
            ],
            [
                'id' => '8',
                'provincia_id' => '2',
                'nome' => 'Baía Farta',
                
            ],
            [
                'id' => '9',
                'provincia_id' => '2',
                'nome' => 'Benguela',
                
            ],  
            [
                'id' => '10',
                'provincia_id' => '2',
                'nome' => 'Bocoio',
            ], 
            [
                'id' => '11',
                'provincia_id' => '2',
                'nome' => 'Caimbambo',
            ], 
            [
                'id' => '12',
                'provincia_id' => '2',
                'nome' => 'Catumbela',
            ], 
            [
                'id' => '13',
                'provincia_id' => '2',
                'nome' => 'Chongorói',
            ], 
            [
                'id' => '14',
                'provincia_id' => '2',
                'nome' => 'Cubal',
            ],
            [
                'id' => '15',
                'provincia_id' => '2',
                'nome' => 'Ganda',
            ],
            [
                'id' => '16',
                'provincia_id' => '2',
                'nome' => 'lobito',
            ],
            [
                'id' => '17',
                'provincia_id' => '3',
                'nome' => 'Andulo',
            ],
            [
                'id' => '18',
                'provincia_id' => '3',
                'nome' => 'Camacupa',
            ],
            [
                'id' => '19',
                'provincia_id' => '3',
                'nome' => 'Catabola',
            ],
            [
                'id' => '20',
                'provincia_id' => '3',
                'nome' => 'Chinguar',
            ],
            [
                'id' => '21',
                'provincia_id' => '3',
                'nome' => 'Chitembo',
            ],
            [
                'id' => '22',
                'provincia_id' => '3',
                'nome' => 'Cuemba',
            ],
            [
                'id' => '23',
                'provincia_id' => '3',
                'nome' => 'Cunhinga',
            ],
            [
                'id' => '24',
                'provincia_id' => '3',
                'nome' => 'Kuito',
            ],
            [
                'id' => '25',
                'provincia_id' => '3',
                'nome' => 'Nharea',
            ],
            [
                'id' => '26',
                'provincia_id' => '4',
                'nome' => 'Belize',
            ],
            [
                'id' => '27',
                'provincia_id' => '4',
                'nome' => 'Buco-Zau',
            ],
            [
                'id' => '28',
                'provincia_id' => '4',
                'nome' => 'Cabinda',
            ],
            [
                'id' => '29',
                'provincia_id' => '4',
                'nome' => 'Cacongo',
            ],
            [
                'id' => '30',
                'provincia_id' => '5',
                'nome' => 'Cahama',
            ],
            [
                'id' => '31',
                'provincia_id' => '5',
                'nome' => 'Cuanhama',
            ],
            [
                'id' => '32',
                'provincia_id' => '5',
                'nome' => 'Curoca',
            ],
            [
                'id' => '33',
                'provincia_id' => '5',
                'nome' => 'Cuvelai',
            ],
            [
                'id' => '34',
                'provincia_id' => '5',
                'nome' => 'Namacunde',
            ],
            [
                'id' => '35',
                'provincia_id' => '5',
                'nome' => 'Ombadja',
            ],
            [
                'id' => '36',
                'provincia_id' => '6',
                'nome' => 'Ambaca',
            ],
            [
                'id' => '37',
                'provincia_id' => '6',
                'nome' => 'Banga',
            ],
            [
                'id' => '38',
                'provincia_id' => '6',
                'nome' => 'Bolongongo',
            ],
            [
                'id' => '39',
                'provincia_id' => '6',
                'nome' => 'Cambambe',
            ],
            [
                'id' => '40',
                'provincia_id' => '6',
                'nome' => 'Cazengo',
            ],
            [
                'id' => '41',
                'provincia_id' => '6',
                'nome' => 'Golungo Alto',
            ],
            [
                'id' => '42',
                'provincia_id' => '6',
                'nome' => 'Gonguembo',
            ],
            [
                'id' => '43',
                'provincia_id' => '6',
                'nome' => 'Lucala',
            ],
            [
                'id' => '44',
                'provincia_id' => '6',
                'nome' => 'Quiculungo',
            ],
            [
                'id' => '45',
                'provincia_id' => '6',
                'nome' => 'Samba Caju',
            ],
            [
                'id' => '46',
                'provincia_id' => '7',
                'nome' => 'Calai',
            ],
            [
                'id' => '47',
                'provincia_id' => '7',
                'nome' => 'Cuangar',
            ],
            [
                'id' => '48',
                'provincia_id' => '7',
                'nome' => 'Cuchi',
            ],
            [
                'id' => '49',
                'provincia_id' => '7',
                'nome' => 'Cuito Cuanavale',
            ],
            [
                'id' => '50',
                'provincia_id' => '7',
                'nome' => 'Dirico',
            ],
            [
                'id' => '51',
                'provincia_id' => '7',
                'nome' => 'Longa',
            ],
            [
                'id' => '52',
                'provincia_id' => '7',
                'nome' => 'Mavinga',
            ],
            [
                'id' => '53',
                'provincia_id' => '7',
                'nome' => 'Menongue',
            ],
            [
                'id' => '54',
                'provincia_id' => '7',
                'nome' => 'Nancova',
            ],
            [
                'id' => '55',
                'provincia_id' => '7',
                'nome' => 'Rivungo ',
            ],
            [
                'id' => '56',
                'provincia_id' => '8',
                'nome' => 'Amboim',
            ],
            [
                'id' => '57',
                'provincia_id' => '8',
                'nome' => 'Cassongue',
            ],
            [
                'id' => '58',
                'provincia_id' => '8',
                'nome' => 'Cela',
            ],
            [
                'id' => '59',
                'provincia_id' => '8',
                'nome' => 'Conda',
            ],
            [
                'id' => '60',
                'provincia_id' => '8',
                'nome' => 'Ebo',
            ],
            [
                'id' => '61',
                'provincia_id' => '8',
                'nome' => 'Libolo',
            ],
            [
                'id' => '62',
                'provincia_id' => '8',
                'nome' => 'Mussende',
            ],
            [
                'id' => '63',
                'provincia_id' => '8',
                'nome' => 'Porto Amboim',
            ],
            [
                'id' => '64',
                'provincia_id' => '8',
                'nome' => 'Quibala',
            ],
            [
                'id' => '65',
                'provincia_id' => '8',
                'nome' => 'Quilenda',
            ],
            [
                'id' => '66',
                'provincia_id' => '8',
                'nome' => 'Seles',
            ],
            [
                'id' => '67',
                'provincia_id' => '8',
                'nome' => 'Sumbe',
            ],
            [
                'id' => '68',
                'provincia_id' => '9',
                'nome' => 'Chipindo',
            ],
            [
                'id' => '69',
                'provincia_id' => '9',
                'nome' => 'Cuvango',
            ],
            [
                'id' => '70',
                'provincia_id' => '9',
                'nome' => 'Humpata',
            ],
            [
                'id' => '71',
                'provincia_id' => '9',
                'nome' => 'Jamba',
            ],
            [
                'id' => '72',
                'provincia_id' => '9',
                'nome' => 'Lubango',
            ],
            [
                'id' => '73',
                'provincia_id' => '9',
                'nome' => 'Matala',
            ],
            [
                'id' => '74',
                'provincia_id' => '9',
                'nome' => 'Quilengues',
            ],
            [
                'id' => '75',
                'provincia_id' => '9',
                'nome' => 'Quipungo ',
            ],
            [
                'id' => '76',
                'provincia_id' => '10',
                'nome' => 'Bailundo',
            ],
            [
                'id' => '77',
                'provincia_id' => '10',
                'nome' => 'Catchiungo',
            ],
            [
                'id' => '78',
                'provincia_id' => '10',
                'nome' => 'Caála',
            ],
            [
                'id' => '79',
                'provincia_id' => '10',
                'nome' => 'Ekunha',
            ],
            [
                'id' => '80',
                'provincia_id' => '10',
                'nome' => 'Huambo',
            ],
            [
                'id' => '81',
                'provincia_id' => '10',
                'nome' => 'Londuimbale',
            ],
            [
                'id' => '82',
                'provincia_id' => '10',
                'nome' => 'Longonjo',
            ],
            [
                'id' => '83',
                'provincia_id' => '10',
                'nome' => 'Mungo',
            ],
            [
                'id' => '84',
                'provincia_id' => '10',
                'nome' => 'Tchicala-Tcholoanga',
            ],
            [
                'id' => '85',
                'provincia_id' => '10',
                'nome' => 'Tchindjenje',
            ],
            [
                'id' => '86',
                'provincia_id' => '10',
                'nome' => 'Ucuma',
            ],
            [
                'id' => '87',
                'provincia_id' => '10',
                'nome' => 'Caconda',
            ],
            [
                'id' => '88',
                'provincia_id' => '10',
                'nome' => 'Cacula',
            ],
            [
                'id' => '89',
                'provincia_id' => '10',
                'nome' => 'Caluquembe',
            ],
            [
                'id' => '90',
                'provincia_id' => '10',
                'nome' => 'Chiange',
            ],
            [
                'id' => '91',
                'provincia_id' => '10',
                'nome' => 'Chibia',
            ],
            [
                'id' => '92',
                'provincia_id' => '10',
                'nome' => 'Chicomba',
            ],
            [
                'id' => '93',
                'provincia_id' => '11',
                'nome' => 'Belas',
            ],
            [
                'id' => '94',
                'provincia_id' => '11',
                'nome' => 'Cacuaco',
            ],
            [
                'id' => '95',
                'provincia_id' => '11',
                'nome' => 'Cazenga',
            ],
            [
                'id' => '96',
                'provincia_id' => '11',
                'nome' => 'Icolo e Bengo',
            ],
            [
                'id' => '97',
                'provincia_id' => '11',
                'nome' => 'Viana',
            ],
            [
                'id' => '98',
                'provincia_id' => '11',
                'nome' => 'Luanda',
            ],
            [
                'id' => '99',
                'provincia_id' => '11',
                'nome' => 'Quiçama',
            ],
            [
                'id' => '100',
                'provincia_id' => '12',
                'nome' => 'Cacolo',
            ],
            [
                'id' => '101',
                'provincia_id' => '12',
                'nome' => 'Dala',
            ],
            
            [
                'id' => '102',
                'provincia_id' => '12',
                'nome' => 'Muconda',
            ],
            [
                'id' => '103',
                'provincia_id' => '12',
                'nome' => 'Saurimo',
            ],
           
            [
                'id' => '104',
                'provincia_id' => '13',
                'nome' => 'Cambulo',
            ],
            [
                'id' => '105',
                'provincia_id' => '13',
                'nome' => 'Capenda-Camulemba',
            ],
            [
                'id' => '106',
                'provincia_id' => '13',
                'nome' => 'Caungula',
            ],
            [
                'id' => '107',
                'provincia_id' => '13',
                'nome' => 'Chitato',
            ],
            [
                'id' => '108',
                'provincia_id' => '13',
                'nome' => 'Cuango',
            ],
            [
                'id' => '109',
                'provincia_id' => '13',
                'nome' => 'Cuílo',
            ],
            [
                'id' => '110',
                'provincia_id' => '13',
                'nome' => 'Lubalo',
            ],
            [
                'id' => '111',
                'provincia_id' => '13',
                'nome' => 'Lucapa',
            ],
            [
                'id' => '112',
                'provincia_id' => '13',
                'nome' => 'Xá-Muteba',
            ],
            [
                'id' => '113',
                'provincia_id' => '14',
                'nome' => 'Cacuso',
            ],
            [
                'id' => '114',
                'provincia_id' => '14',
                'nome' => 'Calandula',
            ],
            [
                'id' => '115',
                'provincia_id' => '14',
                'nome' => 'Cambundi-Catembo',
            ],
            [
                'id' => '116',
                'provincia_id' => '14',
                'nome' => 'Cangandala',
            ],
            [
                'id' => '117',
                'provincia_id' => '14',
                'nome' => 'Caombo',
            ],
            [
                'id' => '118',
                'provincia_id' => '14',
                'nome' => 'Cuaba Nzogo',
            ],
            [
                'id' => '119',
                'provincia_id' => '14',
                'nome' => 'Cunda-Dia-Baze',
            ],
            [
                'id' => '120',
                'provincia_id' => '14',
                'nome' => 'Luquembo',
            ],
            [
                'id' => '121',
                'provincia_id' => '14',
                'nome' => 'Malanje',
            ],
            [
                'id' => '122',
                'provincia_id' => '14',
                'nome' => 'Marimba',
            ],
            [
                'id' => '123',
                'provincia_id' => '14',
                'nome' => 'Massango',
            ],
            [
                'id' => '124',
                'provincia_id' => '14',
                'nome' => 'Mucari',
            ],
            [
                'id' => '125',
                'provincia_id' => '14',
                'nome' => 'Quela',
            ],
            [
                'id' => '126',
                'provincia_id' => '14',
                'nome' => 'Quirima',
            ],
            [
                'id' => '127',
                'provincia_id' => '15',
                'nome' => 'Alto Zambeze',
            ],
           
            [
                'id' => '128',
                'provincia_id' => '15',
                'nome' => 'Bundas',
            ],
            [
                'id' => '129',
                'provincia_id' => '15',
                'nome' => 'Camanongue',
            ],
            [
                'id' => '130',
                'provincia_id' => '15',
                'nome' => 'Léua',
            ],
            [
                'id' => '131',
                'provincia_id' => '15',
                'nome' => 'Luau',
            ],
            [
                'id' => '132',
                'provincia_id' => '15',
                'nome' => 'Luacano',
            ],
            [
                'id' => '133',
                'provincia_id' => '15',
                'nome' => 'Luchazes',
            ],
            [
                'id' => '134',
                'provincia_id' => '15',
                'nome' => 'Luena',
            ],
            [
                'id' => '135',
                'provincia_id' => '15',
                'nome' => 'Lumeje',
            ],
            [
                'id' => '136',
                'provincia_id' => '15',
                'nome' => 'Moxico',
            ],
            [
                'id' => '137',
                'provincia_id' => '16',
                'nome' => 'Bibala',
            ],
            [
                'id' => '138',
                'provincia_id' => '16',
                'nome' => 'Camucuio',
            ],
            [
                'id' => '139',
                'provincia_id' => '16',
                'nome' => 'Moçâmedes',
            ],
            [
                'id' => '140',
                'provincia_id' => '16',
                'nome' => 'Tômbua',
            ],
            [
                'id' => '141',
                'provincia_id' => '16',
                'nome' => 'Virei',
            ],
            [
                'id' => '142',
                'provincia_id' => '17',
                'nome' => 'Alto Cauale',
            ],
            [
                'id' => '143',
                'provincia_id' => '17',
                'nome' => 'Ambuíla',
            ],
            [
                'id' => '144',
                'provincia_id' => '17',
                'nome' => 'Bembe',
            ],
            [
                'id' => '145',
                'provincia_id' => '17',
                'nome' => 'Buengas',
            ],
            [
                'id' => '146',
                'provincia_id' => '17',
                'nome' => 'Bungo',
            ],
            [
                'id' => '147',
                'provincia_id' => '17',
                'nome' => 'Damba',
            ],
            [
                'id' => '148',
                'provincia_id' => '17',
                'nome' => 'Macocola',
            ],
            [
                'id' => '149',
                'provincia_id' => '17',
                'nome' => 'Milunga',
            ],
            [
                'id' => '150',
                'provincia_id' => '17',
                'nome' => 'Mucaba',
            ],
            [
                'id' => '151',
                'provincia_id' => '17',
                'nome' => 'Negage',
            ],
            [
                'id' => '152',
                'provincia_id' => '17',
                'nome' => 'Puri',
            ],
            [
                'id' => '153',
                'provincia_id' => '17',
                'nome' => 'Quimbele',
            ],
            [
                'id' => '154',
                'provincia_id' => '17',
                'nome' => 'Quitexe',
            ],

            [
                'id' => '155',
                'provincia_id' => '18',
                'nome' => 'Cuimba',
            ],
            [
                'id' => '156',
                'provincia_id' => '18',
                'nome' => 'MBanza Kongo',
            ],
            [
                'id' => '157',
                'provincia_id' => '18',
                'nome' => 'Noqui',
            ],
            [
                'id' => '158',
                'provincia_id' => '18',
                'nome' => 'NZeto',
            ],
            [
                'id' => '159',
                'provincia_id' => '18',
                'nome' => 'Soyo',
            ],
            [
                'id' => '160',
                'provincia_id' => '18',
                'nome' => 'Tomboco',
            ],
            

            
        ]);
    }
}

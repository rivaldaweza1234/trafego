<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class EstacaoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('tbl_estacao_trabalho')->insert(
            [
                'tipo' => 'Sede',
                'nome' => 'Luanda',
                'municipio_id' => '98'
            ]
        );
    }
}

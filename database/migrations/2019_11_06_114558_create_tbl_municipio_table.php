<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblMunicipioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_municipio', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedInteger('provincia_id');
            $table->string('nome', 255);
            $table->timestamps();

            $table->foreign('provincia_id')->references('id')->on('tbl_provincia')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_municipio');
    }
}

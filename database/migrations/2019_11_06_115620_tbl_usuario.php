<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblUsuario extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_usuario', function (Blueprint $table) {
            $table->Increments('id');
            $table->string('codigo',255);      
            
            $table->string('nome',255);      
            $table->string('password',255);
            
            $table->enum('nivel',['SuperUsuário','Admin','Usuário','Estatístico']);

            $table->unsignedInteger('id_estacao_trabalho');
           
            $table->timestamps();
            $table->foreign('id_estacao_trabalho')->references('id')->on('tbl_estacao_trabalho');
     

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}

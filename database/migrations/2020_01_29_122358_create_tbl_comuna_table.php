<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblComunaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_comuna', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('estacao_id');
            $table->string('nome', 255);
            $table->timestamps();

            $table->foreign('estacao_id')->references('id')->on('tbl_estacao_trabalho')->onDelete('cascade');
       
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_comuna');
    }
}

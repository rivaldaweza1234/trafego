<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblFormularioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_formulario', function (Blueprint $table) {
            $table->Increments('id');
            
            $table->string('origem'); 
            
            $table->integer('mes');          
            $table->integer('numero_serie');          
            $table->string('envia',500);
            $table->string('recebe',500);
            $table->string('mensagem',2000);
            $table->string('anexo',2000);
            $table->enum('estado', ['Recebido', 'Expedido', 'Invalído'])->default('Expedido');
           
            $table->unsignedInteger('id_tipo_trafego');
            $table->unsignedInteger('id_grau_precedencia'); 
            $table->unsignedInteger('id_usuario');
            $table->unsignedInteger('id_grau_seguranca');
            $table->unsignedInteger('id_estacao_trabalho_recebe');
            
            
            
            $table->timestamps(); 
            $table->foreign('id_tipo_trafego')->references('id')->on('tbl_tipo_trafego');
            $table->foreign('id_grau_precedencia')->references('id')->on('tbl_grau_precedencia');
            $table->foreign('id_estacao_trabalho_recebe')->references('id')->on('tbl_estacao_trabalho'); 
            $table->foreign('id_usuario')->references('id')->on('tbl_usuario'); 
            $table->foreign('id_grau_seguranca')->references('id')->on('tbl_grau_seguranca'); 
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_formulario');
    }
}

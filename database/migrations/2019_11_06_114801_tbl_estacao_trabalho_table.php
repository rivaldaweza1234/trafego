<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TblEstacaoTrabalhoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_estacao_trabalho', function (Blueprint $table) {
            $table->Increments('id');
            $table->unsignedInteger('municipio_id');
            $table->enum('tipo', ['Sede', 'Municipal','Comunal']);
            $table->enum('estado', ['Activa', 'Inactiva'])->default('Activa');
            $table->string('nome',255);
            $table->timestamps();

            $table->foreign('municipio_id')->references('id')->on('tbl_municipio')->onDelete('cascade');
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_estacao_trabalho');
    }
}

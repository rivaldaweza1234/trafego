<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTblOcorrenciaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_ocorrencia', function (Blueprint $table) {
            $table->Increments('id');  
            $table->string('mensagem',255);
            $table->unsignedInteger('id_tipo_ocorrencia');
            $table->timestamps();
            $table->unsignedInteger('id_usuario');

            $table->foreign('id_tipo_ocorrencia')->references('id')->on('tbl_tipo_ocorrencia');
            $table->foreign('id_usuario')->references('id')->on('tbl_usuario');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_ocorrencia');
    }
}

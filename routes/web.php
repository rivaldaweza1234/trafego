<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return redirect('login');
});




Route::get('/estacao', function () {
    return view('estacao');
});


//AUTH
Route::get('/login', 'AuthenController@login')->name('login');


Route::post('/login', 'AuthenController@checkUser');
Route::post('/logout', 'AuthenController@logout');
Route::post('/updatePwd', 'UtilizadoresController@updatePwd');


//Dashboard

Route::get('/pdf/{id}', 'PdfController@geraPdf')->name('gerar-pdf');
Route::get('/pdf2/{id}', 'PdfController@geraPdf2')->name('gerar-pdf2');

Route::get('/dashboard', 'TrafegoController@index')->name('dashboard');

//Ocorrencia
Route::resource('/ocorrencia', 'OcorrenciaController');


//PDF
Route::get('/dar-baixa', 'TrafegoController@darBaixa');

//Utilizador
Route::resource('/utilizador', 'UtilizadoresController');
Route::resource('/grafico', 'GraficoController');
Route::resource('/estacao', 'EstacoesController');
Route::resource('/registo', 'RegistosController');
Route::resource('/escoamento', 'EscoamentoController');

Route::get('/estacao/{estacao}', 'EstacoesController@toggleState')->name('toggle.state');

Route::get('/municipios/{provincia_id}', 'EstacoesController@MunicipiosByProvincia')->name('municipioRequest.get');
Route::get('/comuna/{municipio_id}', 'EstacoesController@ComunaByMunicipio')->name('comunaRequest.get');
Route::get('/grau-precedencia/{grau}', 'EscoamentoController@grauById');
Route::get('/grau-seguranca/{grauSe}', 'EscoamentoController@grauSegurancaById');


Route::get('/recebido/{id}', 'RegistosController@Recebidos');
Route::get('/invalido/{id}', 'RegistosController@Invalidos');

Route::get('/estatistica/occorencia', 'TrafegoController@ocorrencia')->name('statistics.occurs');
Route::get('/estatistica/trafego', 'TrafegoController@trafego')->name('statistics.traffic');
Route::get('/estatistica/estacao', 'TrafegoController@estacao')->name('statistics.estacao');
Route::post('/estatistica/imprimir', 'TrafegoController@print')->name('statistics.print');
//Route::get('/serie/{estacao_id}', 'EscoamentoController@getSerialNumber');

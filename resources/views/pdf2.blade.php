<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body> 
<table cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:none">
	<tbody>
		<tr>
			<td colspan="3" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:1px solid black; height:17px; vertical-align:top; width:127px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><center>	<img src="{{ $_SERVER['DOCUMENT_ROOT'] }}/dist/img/infosi.png" style="width:65%"></center>  </span></span></p>
			</td>
			<td colspan="4" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; height:17px; vertical-align:top; width:309px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">MENSAGEM</span></span></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:1px solid black; height:17px; vertical-align:top; width:129px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Pagina:1</span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:17px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:15px; vertical-align:top; width:564px">
			<p>&nbsp;</p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:15px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:16px; vertical-align:top; width:564px">
			<p>&nbsp;</p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:16px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:16px; vertical-align:top; width:126px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Grau de precedencia  <center>  {{ $escoamento->grau_precedencia->descricao }}</center>  </span></span></p>
			</td>
			<td colspan="4" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:16px; vertical-align:top; width:213px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><center>Data e Hora</center><center> {{ $escoamento->created_at}} </center></span></span></p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:16px; vertical-align:top; width:225px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Serie: {{ $escoamento->numero_serie }}</span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:16px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" rowspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:8px; vertical-align:top; width:339px">
			<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">De: {{ $escoamento->envia }}</span></span></p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:8px; vertical-align:top; width:225px">
			<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Clasificação</span></span></p>
			</td>
			 
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:8px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" rowspan="2" style="border-bottom:none; border-left:none; border-right:1px solid black; border-top:none; height:18px; vertical-align:top; width:225px">
			Numero origem :{{ $escoamento->origem }}
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:18px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" rowspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:18px; vertical-align:top; width:339px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Para:{{ $escoamento->recebe }}</span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:18px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:13px; vertical-align:top; width:225px">
			 	</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:13px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:31px; vertical-align:top; width:564px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">{{ $escoamento->mensagem }}</span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:31px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:60px">
			<p>&nbsp;</p>
			</td>
			<td colspan="3" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:87px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Data</span></span></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:93px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Hora</span></span></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:99px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Operador {{ Auth::user()->nome }}</span></span></p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:225px">
			<p>&nbsp;</p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:25px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:7px; vertical-align:top; width:339px">
			<p>&nbsp;</p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:7px; vertical-align:top; width:225px">
			<p>&nbsp;</p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:7px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:10px; vertical-align:top; width:564px">
			<p>&nbsp;</p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:10px; width:0px">&nbsp;</td>
		</tr>
	</tbody>
</table>



 
 

</body>
</html>
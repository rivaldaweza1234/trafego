@extends('main')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('plugins/select2/css/select2.min.css') }}">
@endsection
@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <strong> SGT</strong>
            <small><b>Sistema de Gestão de Tráfego</b></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard </a></li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Relátorio - Tráfego</h3>
                        </div>
                        <form action="{{ route('statistics.print') }}" method="POST">
                            <div class="col-md-12 bg-gray-light">
                                <div class="form-group">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                </div>
                            </div>



                            <div class="col-md-2">


                                <div class="form-group">
                                    <label>Estacao</label>
                                    <select name="estacao_id"class="form-control" required>
                                        <option selected value="">Selecione a provincia</option>
                                        @foreach($provincia as $provincia)
                                        <option value="{{ $provincia->id }}">{{ $provincia->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div> 
                            <br> 
                            <br> 
                            

                            <div class="col-md-2">
                                <div class="form-group">

                                    <button type="submit" class="btn btn-primary form-control"> <i class="fa fa-print"></i> Imprimir</button>
                                </div>
                            </div>
                        </form>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tipo</th>
                                                    <th>Nome</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $a = 1; ?>
                                                @foreach($estacao as $estacao)
                                                <tr>
                                                    <td>{{ $a }}</td>
                                                    <td>{{ $estacao->tipo }}

                                                    <td>{{ $estacao->nome }}

                                                </tr>
                                                <?php $a++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

</div>
@endsection
@section('js')
<script src="{{ URL::asset('plugins/jquery/jquery.min.js') }}"></script>

<script src="{{ URL::asset('plugins/select2/js/select2.full.min.js') }}"></script>

<script>
    $(document).on('change', '.select-municipio', function() {
        $("[name=comuna_id]").html('<option selected disabled value="">Selecione um Comuna</option>');
        let muni_id = $(this).val();
        $.ajax({
            'async': false,
            'type': "GET",
            'global': false,
            'dataType': 'json',
            'url': "/public/comuna/" + muni_id,
            'data': {
                'request': "",
                'target': 'arrange_url',
                'method': 'method_target'
            },
            'success': function(data) {
                //console.log(data);
                for (var i = 0; i < data.length; i++) {
                    $("[name=comuna_id]").append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                }
            }
        });
    });



    $(document).on('change', '.select-provincia', function() {
        $("[name=municipio_id]").html('<option selected disabled value="">Selecione um município</option>');
        let prov_id = $(this).val();
        $.ajax({
            'async': false,
            'type': "GET",
            'global': false,
            'dataType': 'json',
            'url': "/public/municipios/" + prov_id,
            'data': {
                'request': "",
                'target': 'arrange_url',
                'method': 'method_target'
            },
            'success': function(data) {
                //console.log(data);
                for (var i = 0; i < data.length; i++) {
                    $("[name=municipio_id]").append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                }
            }
        });
    });

    $(function() {
        //Initialize Select2 Elements
        $('.select2').select2({
            theme: 'bootstrap4',
            placeholder: 'Selecione pelo menos uma província'
        })

    })
</script>
<script type="text/javascript">
    $("[name=data_fim]").attr({
        "min": $("[name=data_inicio]").val()
    });
    $("[name=data_fim]").val($("[name=data_inicio]").val());


    $("[name=data_inicio]").on("change", function(e) {

        $("[name=data_fim]").val($(this).val());
        $("[name=data_fim]").attr({
            "min": $(this).val()
        });

    });

    $("[name=mes]").attr("disabled", "disabled");
    $("[name=ano]").attr("disabled", "disabled");

    $("#mes_ano_toogle").on("click", function(e) {
        if ($("#mes_ano_toogle").is(":checked")) {

            $("[name=mes]").removeAttr("disabled");
            $("[name=ano]").removeAttr("disabled");

        } else {

            $("[name=mes]").attr("disabled", "disabled");
            $("[name=ano]").attr("disabled", "disabled");

        }

    });
</script>
@endsection
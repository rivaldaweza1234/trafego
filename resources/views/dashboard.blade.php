 @extends('main')

 @section('main-content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
        <h1>
        <strong> SGT</strong>
       <small><b>Sistema de Gestão de Tráfego</b></small>
     </h1>
     <ol class="breadcrumb">
       <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active">Dashboard </a></li>
     </ol>
   </section>
      
 
   <!-- Main content -->
   <section class="content">
   <div class="row">
   <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
            <h4> Total Registo<sup style="font-size: 20px"></sup></h4>
              <span class="info-box-number">{{ $totalAceitacao }}</span>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
           
            <a href="#" class="small-box-footer">#  </a>
          </div>
        </div>


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
               
              <h4> Total Escoamento<sup style="font-size: 20px"></sup></h4>
              <span class="info-box-number">{{ $totalEscoamento}}</span>
             
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
         
            <a href="#" class="small-box-footer"># </a>
          </div>
        </div>


        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
            <h4> Total Ocorrencia<sup style="font-size: 20px"></sup></h4>
              <span class="info-box-number">{{ $totalOcorrencia}}</span>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
       
            <a href="#" class="small-box-footer"># </a>
          </div>
        </div>
        <!-- ./col -->

        @if(Auth::user()->nivel == "SuperUsuário" || Auth::user()->nivel == "Admin")


        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
            <h4> Total Usuários<sup style="font-size: 20px"></sup></h4>
              <span class="info-box-number">{{ $totalUsuarios}}</span>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="#" class="small-box-footer">#</a>
          </div>
        </div>
        <!-- ./col -->
      </div>

          @endif

       
    <!-- Main row -->
   

     
      <!-- /.row -->
    </section>
      
    </div>
    <!-- /.row (main row) -->

  </section>


 
        <!-- /.col -->
        
        <!-- /.col -->

        
        

          <!-- /.info-box -->
        </div>
        <!-- /.col -->
      </div>

   </section>
   <!-- /.content -->
 </div>
 @endsection
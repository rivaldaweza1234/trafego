@extends('main')

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <strong> SGT</strong>
            <small><b>Sistema de Gestão de Tráfego</b></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard </a></li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Relátorio - Tráfego</h3>
                        </div>
                        <form action="{{ route('statistics.print') }}" method="POST">
                            <div class="col-md-12 bg-gray-light">
                                <div class="form-group">
                                    <label> Sem filtro</label>
                                    <input type="checkbox" name="all">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Tipo Trafego</label>
                                    <select name="tipo_trafego_id" id="" class="form-control">
                                        <option value="">Seleciona uma opção </option>
                                        @foreach( $trafego as $tf)
                                        <option value="{{ $tf->id }}">{{ $tf->descricao}} </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Grau seguranca</label>
                                    <select name="grau_seguranca_id" id="" class="form-control">
                                        <option value="">Seleciona uma opção </option>
                                        @foreach( $seguranca as $gs)
                                        <option value="{{ $gs->id }}">{{ $gs->descricao}} </option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Grau Precedencia</label>
                                    <select name="grau_precedencia_id" id="" class="form-control">
                                        <option value="">Seleciona uma opção </option>
                                        @foreach( $grau as $gp)
                                        <option value="{{ $gp->id }}">{{ $gp->descricao}} </option>
                                        @endforeach
                                    </select>

                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Estado</label>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <select name="estado_registo" class="form-control">
                                        <option selected disabled value="">Seleciona uma opção</option>

                                        <option value="Invalído">Inválido(s)</option>
                                        <option value="Expedido">Expedida(s)</option>
                                        <option value="Recebido">Confirmada(s)</option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Estação de Origem</label>
                                    <select name="id_estacao_trabalho_origem" id="" class="form-control chosen-select">
                                        <option value="">Seleciona uma opção</option>
                                        @foreach( $estacoes as $estacao)
                                        <option value="{{ $estacao->id }}">{{$estacao->municipio->provincia->codigo}} - {{$estacao->nome}} ({{$estacao->tipo}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Estação de Destino</label>
                                    <select name="id_estacao_trabalho_destino" id="" class="form-control chosen-select">
                                        <option value="">Seleciona uma opção</option>
                                        @foreach( $estacoes as $estacao)
                                        <option value="{{ $estacao->id }}">{{$estacao->municipio->provincia->codigo}} - {{$estacao->nome}} ({{$estacao->tipo}})</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Início</label>
                                    <input type="date" name="data_inicio" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Término</label>
                                    <input type="date" name="data_fim" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-2">

                                <div class="form-group">
                                    <label>Mês <input type="checkbox" id="mes_toogle"></label>
                                    <select name="mes" class="form-control">
                                        @foreach($months as $key => $month)
                                        <option value="{{ $key }}">{{ $month }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Ano <input type="checkbox" id="ano_toogle"></label>
                                    <select name="ano" class="form-control">
                                        @foreach($years as $key => $year)
                                        <option value="{{ $key }}">{{ $year }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>&nbsp;</label>
                                    <button type="submit" class="btn btn-primary form-control"> <i class="fa fa-print"></i> Imprimir</button>
                                </div>
                            </div>
                        </form>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Emissor</th>
                                                    <th>Receptor</th>
                                                    <th>Serie</th>
                                                    <th>Estação de Origem</th>
                                                    <th>Estação de Destino</th>
                                                    <th class="text-center">Ação</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $a = 1; ?>
                                                @foreach($registos as $registo)
                                                <tr>
                                                    <td>{{ $a }}</td>
                                                    <td>{{ $registo->envia }}</td>
                                                    <td>{{ $registo->recebe }}</td>
                                                    <td>{{ $registo->numero_serie }}</td>
                                                    <td>{{ $registo->usuario->estacao->municipio->provincia->codigo }} - {{ $registo->usuario->estacao->nome }} ({{ $registo->usuario->estacao->tipo }})</td>
                                                    <td>{{ $registo->estacao->municipio->provincia->codigo }} - {{ $registo->estacao->nome }} ({{ $registo->estacao->tipo }})</td>
                                                    <td class="text-center">
                                                        <a class="btn btn-primary btn-sm" href="">Visualizar</a>
                                                    </td>
                                                </tr>
                                                <?php $a++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

</div>
@endsection
@section('js')
<script type="text/javascript">
    $("[name=data_fim]").attr({
        "min": $("[name=data_inicio]").val()
    });
    $("[name=data_fim]").val($("[name=data_inicio]").val());


    $("[name=data_inicio]").on("change", function(e) {

        $("[name=data_fim]").val($(this).val());
        $("[name=data_fim]").attr({
            "min": $(this).val()
        });

    });

    $("[name=mes]").attr("disabled", "disabled");
    $("[name=ano]").attr("disabled", "disabled");

    $("#mes_ano_toogle").on("click", function(e) {
        if ($("#mes_ano_toogle").is(":checked")) {

            $("[name=mes]").removeAttr("disabled");
            $("[name=ano]").removeAttr("disabled");

        } else {

            $("[name=mes]").attr("disabled", "disabled");
            $("[name=ano]").attr("disabled", "disabled");

        }

    });
</script>
@endsection
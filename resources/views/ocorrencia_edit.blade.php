@extends('main')

@section('css')
    
  <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

@endsection

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SGT
      <small>Sistema de Gestão de Tráfego</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Ocorrência</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Ocorrência</h3>
            
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action="{{ route('ocorrencia.update', $ocorrencia->id) }}" method="POST" role="form">
          @method('PUT')
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            <div class="box-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

           

            <div class="col-md-4">
                <div class="form-group">
                  <label>Tipo Ocorrencia</label>
                  <select name="tipo_ocorrencia_id" id="" class="form-control" required>
                  <option value="">Seleciona o Tipo de Ocorrencia</option>
                    @foreach( $tipoOcorrencia as $oc)
                      <option @if($oc->id == $ocorrencia->id_tipo_ocorrencia) selected @endif value="{{ $oc->id }}">{{ $oc->descricao}} </option>
                    @endforeach
                  </select>  

                </div>
               </div>

              <div class="col-md-12">
                <div class="form-group">
                  <label>Ocorrências</label>

                  <textarea name="mensagem" id="" cols="30" rows="10" class="textarea form-control" required>  {{ $ocorrencia->mensagem }}   </textarea>
                </div>
              </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer text-right">
              <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
@endsection

@section('js')
    
<!-- Bootstrap WYSIHTML5 -->
<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>


@endsection
<!DOCTYPE html>
<html>

<!-- Mirrored from adminlte.io/themes/AdminLTE/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 14:14:21 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <title>SGT | INFOSI </title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ URL::asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ URL::asset('bower_components/font-awesome/css/font-awesome.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ URL::asset('bower_components/Ionicons/css/ionicons.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.min.css') }}">

  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ URL::asset('dist/css/skins/_all-skins.min.css') }}">

  <link rel="stylesheet" href="{{ URL::asset('dist/css/novo.css') }}">

  <!-- Chosen -->
  <link rel="stylesheet" href="{{ URL::asset('bower_components/chosen/chosen.min.css') }}">

  <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
  <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />

  @yield('css')

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

</head>

<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">

      <!-- Logo -->

      <a href="/dashboard" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"> <img src="img/LOGO.png"></span>
        <!-- logo for regular state and mobile devices -->
        <!--   <span class="logo-lg">
          <b>SGr</b> - TRÁFEGO</span> -->

        <span class="logo-lg">

          <img src="img/LOGO2.png">
        </span>

      </a>



      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">

        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">

          <span class="sr-only">Toggle navigation</span>


        </a>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">




          <ul class="nav navbar-nav">
            <li>

              <div class="text-right" style="margin-top: 5px;">
                <h4 class="badge badge-danger bg-blue" style="text-transform: uppercase;">Operador: {{ Auth::user()->codigo }} - {{ Auth::user()->nome }} | Posição: {{ Auth::user()->estacao->nome }} </h4>
              </div>
            </li>
            <li>

              <div class="badge bg-blue" style="margin-top: 15px;  float:right;">
                Data: {{ date('d:m:y') }}
              </div>


            </li>

            <!-- Power off-->
            <li class="">
              <form id="logout" action="{{ url('logout') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}" />
              </form>
              <a href="javascript:;" id="sair">
                <i class="fa fa-power-off"></i>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>

    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left info">
            <div class="pull-left image">

              <p>{{ Auth::user()->nome }}</p>
            </div>
          </div>

          <br>


        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        
        <ul class="sidebar-menu" data-widget="tree">
        @if(Auth::user()->nivel == "SuperUsuário" || Auth::user()->nivel == "Admin"  || Auth::user()->nivel == "Usuário")

          <li class="header" style="color: #fff;">MENU PRINCIPAL</li>
          <li class="{{ request()->routeIs('dashboard') ? 'active' : '' }}">
            <a href="/dashboard">
              <i class="fa fa-dashboard" style="color: #fff;"></i> <span style="color: #fff;">Dashboard</span>
            </a>
          </li>
          <li>
          <li class="treeview {{ request()->routeIs('ocorrencia*') ? 'active' : '' }}">
            <a href="#">
              <i class="fa fa-edit" style="color: #fff;"></i> <span style="color: #fff;"> Ocorrencia</span>
              <span class="pull-right-container" style="color: #fff;">
                <i class="fa fa-angle-left pull-right" style="color: #fff;"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="{{ request()->routeIs('ocorrencia.create') ? 'active' : '' }}"><a href="{{ route('ocorrencia.create') }}"><i class="fa fa-circle-o"></i>Novo</a></li>
              <li class="{{ request()->routeIs('ocorrencia.index') ? 'active' : '' }}"><a href="{{ route('ocorrencia.index') }}"><i class="fa fa-circle-o"></i> Listar</a></li>

            </ul>
          </li>
          <li class="treeview  {{ request()->routeIs('escoamento*') ? 'active' : '' }}">
            <a href="#">
              <i class="fa fa-edit" style="color: #fff;"></i> <span style="color: #fff;">Escoamento de Tráfego</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right" style="color: #fff;"></i>
              </span>
            </a>
            <ul class="treeview-menu">


              <li class="{{ request()->routeIs('escoamento.index') ? 'active' : '' }}  "><a href="{{ url('escoamento') }}"><i class="fa fa-circle-o"></i> Expedir</a></li>
              <li class="{{ request()->routeIs('registo.index') ? 'active' : '' }}"><a href="{{ url('registo') }}"><i class="fa fa-circle-o"></i> Receber</a></li>

            </ul>
          </li>
          @endif


          <li>

            <!--<li class="treeview {{ request()->routeIs('registo*') ? 'active' : '' }}">
            <a href="#">
              <i class="fa fa-edit"></i> <span>Aceitação e Registo</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">

              <li class="{{ request()->routeIs('registo.create') ? 'active' : '' }}"><a href="{{ route('registo.create') }}"><i class="fa fa-circle-o"></i>Novo</a></li>
              <li class="{{ request()->routeIs('registo.index') ? 'active' : '' }}"><a href="{{ url('registo') }}"><i class="fa fa-circle-o"></i> Listar</a></li>

            </ul>
          </li>-->



            @if(Auth::user()->nivel == "SuperUsuário" || Auth::user()->nivel == "Admin" )
          <li class="{{ request()->routeIs('utilizador*') ? 'active' : '' }}">
            <a href="{{ route('utilizador.index') }}" style="color: #fff;">
              <i class="fa fa-users" style="color: #fff;"></i>
              <span>Utilizadores</span>
            </a>
          </li>
          @endif


          @if(Auth::user()->nivel == "SuperUsuário" || Auth::user()->nivel == "Admin" )
          <li class="{{ request()->routeIs('estacao*') ? 'active' : '' }}">
            <a href="{{ route('estacao.index') }}" style="color: #fff;">
              <i class="fa fa-home" style="color: #fff;"></i>
              <span>Estações</span>
            </a>
          </li>
          @endif

          @if(Auth::user()->nivel == "Estatístico")
          <li class="treeview  {{ (request()->routeIs('statistics.occurs') || request()->routeIs('statistics.traffic')) ? 'active' : '' }}">
            <a href="#">
              <i class="fa fa-edit" style="color: #fff;"></i> <span style="color: #fff;">Estatística</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right" style="color: #fff;"></i>
              </span>
            </a>
            <ul class="treeview-menu">
          
              <li class="{{ request()->routeIs('statistics.occurs') ? 'active' : '' }}  "><a href="{{ route('statistics.occurs') }}"><i class="fa fa-circle-o"></i> Occorência</a></li>
              <li class="{{ request()->routeIs('statistics.traffic') ? 'active' : '' }}"><a href="{{ route('statistics.traffic') }}"><i class="fa fa-circle-o"></i> Tráfego</a></li>
              <li class="{{ request()->routeIs('statistics.estacao') ? 'active' : '' }}"><a href="{{ route('statistics.estacao') }}"><i class="fa fa-circle-o"></i> Estacao</a></li>
        
            </ul>
          </li>
          @endif

          <div class="divisor">
            <hr>
          </div>


          <li class="">

            <a href="http://infocloud.gov.ao/index.php/login" target="_blank" style="color: #fff;">
              <i class="fa fa-save"></i>
              <span>INFOCLOUD</span>
            </a>
          </li>

          <li class="">

            <a href="https://infosi.gov.ao/" target="_blank" style="color: #fff;">
              <i class="fa fa-envelope-o"></i>
              <span>Email Institucional</span>
            </a>
          </li>
          <li class="">
            <a href="manual/manual.pdf" style="color: #fff;">
              <i class="fa fa-dashboard" style="color: #fff;"></i> <span>
                Manual de utilização</span>
            </a>
          </li>
          <li class="bg-red">

            <a href="#" data-toggle="modal" data-target="#myModal" style="color: #fff;">
              <i class="fa fa-key"></i>
              <span>Alterar Senha</span>
            </a>
          </li>
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    @yield('main-content')
    @yield('modal')
    <!-- /.content-wrapper -->
    <!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center">Alterar Senha</h4>
          </div>
          <div class="modal-body">
            <div id="form_result"></div>
            <form id="sample_form" method="POST">
              <div class="form-group">
                <label for="">Senha Actual</label>
                <input type="password" name="password" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Nova Senha</label>
                <input type="password" name="new_password" class="form-control">
              </div>
              <div class="form-group">
                <label for="">Confirmar Nova Senha</label>
                <input type="password" name="new_password_confirm" class="form-control">
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary btn-submit">Alterar</button>
          </div>
        </div>

      </div>
    </div>

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Versão</b> 1.0.0
      </div>
      <strong>Copyright &copy; <?php echo (date("Y") == "2019") ? date("Y") : "2019 - " . date("Y");  ?> <a href="#">SGT - INFOSI</a>.</strong> Todos os direitos reservados.
    </footer>

  </div>

  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="{{ URL::asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ URL::asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
  <!-- Select2 -->
  <script src="{{ URL::asset('bower_components/chosen/chosen.jquery.min.js') }}"></script>
  <!-- FastClick -->
  <script src="{{ URL::asset('bower_components/fastclick/lib/fastclick.js') }}"></script>
  <!-- AdminLTE App -->
  <script src="{{ URL::asset('dist/js/adminlte.min.js') }}"></script>
  <!-- Sparkline -->
  <script src="{{ URL::asset('bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
  <!-- jvectormap  -->
  <script src="{{ URL::asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
  <script src="{{ URL::asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
  <!-- SlimScroll -->
  <script src="{{ URL::asset('bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ URL::asset('dist/js/demo.js') }}"></script>
  <!-- Page script -->

  <script src="{{ URL::asset('bower_components/ckeditor/ckeditor.js') }}"></script>
  <script>
    $(function() {
      // Replace the <textarea id="editor1"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace()
      //bootstrap WYSIHTML5 - text editor
      $('.textarea1').wysihtml5()
    })
  </script>



  <script>
    $(function() {
      //Initialize Chosen Elements
      $(".chosen-select").chosen({
        width: "100%",
        placeholder_text_single: "Selecione uma opção...",
        no_results_text: "Nenhuma correspondência de resultados:"
      });

    });

    $('#sair').click(function() {

      $("#logout").submit();

    })
  </script>

  <!-- Credecials script -->
  <script type="text/javascript">
    $.ajaxSetup({

      headers: {

        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

      }

    });

    $(".btn-submit").click(function(e) {

      e.preventDefault();


      var password = $("input[name=password]").val();

      var new_password = $("input[name=new_password]").val();

      var new_password_confirm = $("input[name=new_password_confirm]").val();


      $.ajax({

        type: 'POST',

        url: '/updatePwd',

        data: {
          password: password,
          new_password: new_password,
          new_password_confirm: new_password_confirm
        },

        success: function(data) {

          var msg = '';
          if (data.errors) {
            msg = '<div class="alert alert-danger text-center">';

            for (var count = 0; count < data.errors.length; count++) {
              msg += '<p>' + data.errors[count] + '</p>';
            }

            msg += '</div>';

            $('#form_result').html(msg);
          }

          if (data.error) {

            msg = '<div class="alert alert-danger text-center">' + data.error + '</div>';
            $('#form_result').html(msg);

          }

          if (data.success) {

            msg = '<div class="alert alert-success text-center">' + data.success + '</div>';
            $('#sample_form')[0].reset();

            $('#form_result').html(msg);

            setTimeout(() => {
              $('#sair').click();
            }, 3000);

          }

          //console.log(data.error);

        }

      });

    });
  </script>

  @yield('js')

</body>

<!-- Mirrored from adminlte.io/themes/AdminLTE/index2.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 05 Nov 2019 14:14:47 GMT -->

</html>
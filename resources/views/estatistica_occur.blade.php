@extends('main')

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <strong> SGT</strong>
            <small><b>Sistema de Gestão de Tráfego</b></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard </a></li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Relátorio - Ocorrencia</h3>
                        </div>
                        <form action="{{ route('statistics.print') }}" method="POST">
                            <div class="col-md-12">
                              
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Tipo Ocorrencia</label>
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="type" value="occur">

                                    <select name="tipo_ocorrencia_id" class="form-control">
                                        <option selected disabled value="">Seleciona uma opção</option>
                                        @foreach($tipoOcorrencias as $tipo)

                                        <option value="{{ $tipo->id }}">{{ $tipo->descricao }}</option>

                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Inicio</label>
                                    <input type="date" name="data_inicio" class="form-control" value="{{ date('Y-m-d') }}">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Fim</label>
                                    <input type="date" name="data_fim" class="form-control">
                                </div>
                            </div>
                          
                            <div class="col-md-3">
                                <div class="form-group">
                              
                                <button style="margin-top: 23px;" type="submit" class="btn btn-primary">Imprimir</button>
                            </div>
                            
                            </div>
                        </form>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Tipo</th>
                                                    <th>Utilizador</th>
                                                   
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $a = 1; ?>
                                                @foreach($ocorrencias as $ocorrencia)
                                                <tr>
                                                    <td>{{ $a }}</td>
                                                    <td>{{ $ocorrencia->tipo_ocorrencia->descricao }}</td>
                                                    <td>{{ $ocorrencia->usuario->nome }}</td>
                                                   
                                                </tr>
                                                <?php $a++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

</div>
<!-- /.row (main row) -->

@endsection
@section('js')

<script type="text/javascript">
    $("[name=data_fim]").attr({
        "min": $("[name=data_inicio]").val()
    });
    $("[name=data_fim]").val($("[name=data_inicio]").val());


    $("[name=data_inicio]").on("change", function(e) {

        $("[name=data_fim]").val($(this).val());
        $("[name=data_fim]").attr({
            "min": $(this).val()
        });

    });

    $("[name=mes]").attr("disabled", "disabled");
    $("[name=ano]").attr("disabled", "disabled");

    $("#mes_ano_toogle").on("click", function(e) {
        if ($("#mes_ano_toogle").is(":checked")) {

            $("[name=mes]").removeAttr("disabled");
            $("[name=ano]").removeAttr("disabled");

        } else {

            $("[name=mes]").attr("disabled", "disabled");
            $("[name=ano]").attr("disabled", "disabled");

        }

    });
</script>
@endsection
@extends('main')


@section('css')

<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

@endsection

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SGT
            <small>Sistema de Gestão de Tráfego</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Lista Registos</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @elseif(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Recebidas</h3>

                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>
                                            <tr>

                                                <th>#</th>
                                                <th>Emissor</th>
                                                <th>Receptor</th>
                                                <th>Serie</th>
                                                <th>Origem</th>
                                                <th>Enviado da Provincia</th>
                                                <th>Recebido da Provincia</th>
                                                <th>Anexo</th>
                                                <th class="text-center">Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>



                                            <?php $a = 1; ?>
                                            @foreach($registo as $registo)

                                            @if( $registo->id_estacao_trabalho_recebe == Auth::user()->estacao->id )
                                            <tr @if($registo->estado == "Invalído") style="background: tomato; color: white;" @endif>

                                                <td>{{ $a }}</td>
                                                <td>{{ $registo->envia }}</td>
                                                <td>{{ $registo->recebe }}</td>
                                                <td>{{ $registo->numero_serie }}</td>
                                                <td>{{ $registo->origem }}</td>

                                                <td> {{ $registo->usuario->estacao->nome }}</td>
                                                <td> {{ $registo->estacao->nome }}</td>

                                                <td> <a @if($registo->estado == "Invalído") style="color: white;" @endif target="_blank" href="/storage/app/{{ $registo->anexo}}"> Download </a></td>

                                                <td class="text-center">


                                                    @if($registo->estado != "Invalído")

                                                    <a class="btn btn-sm btn-danger" onclick="InvalidarEscoamento(<?= $registo->id ?>)" href="#">Invalidar</a>

                                                        @if($registo->estado != "Recebido")
                                                            <a class="btn btn-sm btn-warning" href="{{ route('gerar-pdf2', $registo->id) }}">Dar baixa</a>
                                                            <a class="btn btn-sm btn-warning" onclick="ImprimirRecibo(<?= $registo->id ?>)" href="#">Recibo</a>
                                                        @endif
                                                    @endif

                                                </td>

                                            </tr>
                                            @endif
                                            <?php $a++; ?>
                                            @endforeach

                                        </tbody>

                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
@endsection
@section('js')

<script type="text/javascript">
    function InvalidarEscoamento(id) {
        $.ajax({
            'async': false,
            'type': "GET",
            'global': false,
            'dataType': 'json',
            'url': "/public/invalido/" + id,
            'data': {
                'request': "",
                'target': 'arrange_url',
                'method': 'method_target'
            },
            'success': function(data) {
                //console.log(data);
                if (data['success']) {

                    location.reload();

                }
            }
        });
    }

    function ImprimirRecibo(id) {
        //Reservado para função de recebido
        $.ajax({
            'async': false,
            'type': "GET",
            'global': false,
            'dataType': 'json',
            'url': "/public/recebido/" + id,
            'data': {
                'request': "",
                'target': 'arrange_url',
                'method': 'method_target'
            },
            'success': function(data) {
                //console.log(data);
                if (data['success']) {

                    window.open('/public/pdf/' + id, '_blank');
                    location.reload();

                }
            }
        });
    }
</script>

<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- page script -->
<script>
    $(function() {
        $('#example1').DataTable()
    })
</script>

@endsection
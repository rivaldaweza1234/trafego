@extends('main')

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
     <h1>
       SGT
      <small>Sistema de Gestão de Tráfego</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Utilizadores</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Novo Utilizador</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="{{ route('utilizador.update', $utilizador->id) }}" method="POST">
            @method('PUT')
            <div class="box-body">
              <div class="col-md-4">
               <div class="form-group">
                 <label>Codigo</label>
                 <input type="hidden" name="_token" value="{{ csrf_token() }}">
                 <input type="text" name="codigo" disabled value="{{ $utilizador->codigo }}" class="form-control">
               </div>
              </div>

              <div class="col-md-4">
               <div class="form-group">
                 <label>Nome</label>
                 <input type="text" name="nome" value="{{ $utilizador->nome }}" class="form-control" required>
               </div>
              </div>

              <div class="col-md-4">
               <div class="form-group">
                 <label>Nova Senha (Opcional)</label>
                 <input type="text" name="password" class="form-control">                  
               </div>
              </div>

              @if(Auth::user()->nivel=='Admin' )
          
            <div class="col-md-4">
              <div class="form-group">
                <label>Nível de Acesso:</label>
                <input type="text" name="nivel" readonly value="Usuário" class="form-control">
              </div>
            </div>
              

          @else  
        
         <div class="col-md-4">
           <div class="form-group">
             <label>Nível de Acesso</label>
             <select name="nivel" class="form-control" required>
               <option selected disabled value="">Selecione o nível de acesso</option>
               <option @if($utilizador->nivel == 'Admin') selected @endif value="Admin">Admin</option>
               <option @if($utilizador->nivel == 'Usuário') selected @endif value="Usuário">Usuário</option>
               <option @if($utilizador->nivel == 'SuperUsuário') selected @endif value="SuperUsuário">Super Usuário</option>
             </select>                  
           </div>
          </div>
       
        @endif

        <div class="col-md-4">
        <div class="form-group">
          <label>Estação de Trabalho</label>
          <select name="id_estacao_trabalho" class="form-control chosen-select" required>
            <option selected disabled value="">Selecione a estação</option>
            @foreach($estacoes as $estacao)
              @if(Auth::user()->nivel=='Admin')

                @if(Auth::user()->estacao->municipio->provincia->id == $estacao->municipio->provincia->id)
                  <option @if($utilizador->id_estacao_trabalho == $estacao->id) selected @endif value="{{ $estacao->id }}">{{ $estacao->municipio->provincia->codigo }} - {{ $estacao->nome }} ({{ $estacao->tipo }})</option>
                @endif

              @else
                <option @if($utilizador->id_estacao_trabalho == $estacao->id) selected @endif value="{{ $estacao->id }}">{{ $estacao->municipio->provincia->codigo }} - {{ $estacao->nome }} ({{ $estacao->tipo }})</option>
              @endif
            @endforeach
          </select>                  
        </div>
        </div>
        </div>

            </div>
            <!-- /.box-body -->

            <div class="box-footer text-right">
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
@endsection
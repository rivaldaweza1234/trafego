@extends('main')

@section('css')

<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

@endsection

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SGT
            <small>Sistema de Gestão de Tráfego</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Lista Ocorrencia</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
             
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Lista de Ocorrencias</h3>
                        
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>
                                            <tr>
                                               
                                                <th>#</th>
                                                <th>Tipo de Ocorrencia</th>
                                                <th>Usuário</th>
                                                <th>Data</th>
                                              <th class="text-center">Ação</th>  
                                            </tr>
                                        </thead>
 

 
                                        <tbody>
                                            {{ $a = 1 }}
                                            @foreach($ocorrencias as $ocorrencia)
                                                      
                                            
                                                @if($ocorrencia->id_usuario == Auth::user()->id)
                                                       
                                                    <tr>    
                                                        <td>{{ $a }}</td>
                                                        <td>{{ $ocorrencia->tipo_ocorrencia->descricao }}</td>
                                                        <td>{{ $ocorrencia->usuario->nome }}</td> 
                                                        <td>{{ $ocorrencia->created_at }}</td> 
                                                        <td class="text-center">
                                                            
                                                        <form action="{{ route('ocorrencia.destroy', $ocorrencia->id) }}" method="POST">
                                                                @method('DELETE')
                                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                        
                                                                        <a href="{{ $ocorrencia->id }}" class="btn btn-sm btn-success" data-toggle="modal" data-target="#exampleModal-{{  $ocorrencia->id }}">  <i class="fa fa-eye"></i>  </a>
                                                                <a href="{{ route('ocorrencia.edit', $ocorrencia->id) }}" class="btn btn-sm btn-primary"> Editar </a>

                                                                <button class="btn btn-sm btn-danger">Excluir</button>
                                                            </form>
                                                        </td>
                                                    </tr>                                                 
                                                @endif  
                                            {{ $a++ }}

                                                                                    
                                    
                                <div class="modal fade" id="exampleModal-{{  $ocorrencia->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Ocorrencia</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        
                                    {{ $ocorrencia->mensagem }}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-danger" data-dismiss="modal">Sair</button>
                                    
                                    </div>
                                    </div>
                                </div>
                                </div>
       
                                            @endforeach
                                        </tbody>
                                    
                                    </table>
</div><br> <br> <br> <br> <br>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->

    </section>
    
                      <!-- Modal -->


                       
       
@endsection
@section('js')

<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- page script -->
<script>
    $(function() {
        $('#example1').DataTable()
    })
</script>

@endsection
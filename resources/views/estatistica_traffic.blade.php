@extends('main')

@section('css')
<link rel="stylesheet" href="{{ URL::asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ URL::asset('plugins/select2/css/select2.min.css') }}">
@endsection
@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <strong> SGT</strong>
            <small><b>Sistema de Gestão de Tráfego</b></small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard </a></li>
        </ol>
    </section>


    <!-- Main content -->
    <section class="content">

        <section class="content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="box box-primary">
                        <div class="box-header">
                            <h3 class="box-title">Relátorio - Tráfego</h3>
                            <br>


                        </div>

                        <form action="{{ route('statistics.print') }}" method="POST">

                            @if(Auth::user()->nivel=='SuperUsuário' )
                            <div class="form-group">
                                <label> Todas províncias</label>
                                <input type="checkbox" name="all">
                            </div>
                            @endif

                            <div class="col-md-12 bg-gray-light">
                                <div class="form-group">

                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                                </div>
                            </div>

                            @if(Auth::user()->nivel=='SuperUsuário' )
                            <div class="col-md-4 provincia_multiple ">
                                <div class="form-group">
                                    <label>Província(s)</label>
                                    <select name="provincia_origem_multiple[]" class="select2" multiple="multiple" id="" class="form-control " style="width: 100%;">required>
                                        @foreach($provincias as $provincia)
                                        <option value="{{ $provincia->id }}">{{ $provincia->nome }} - {{ $provincia->codigo }}</option>
                                        @endforeach

                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 provincia">
                                <div class="form-group">
                                    <label>Província</label>
                                    <select name="provincia_origem[]" class="form-control select-provincia">
                                        <option selected disabled value="">Selecione a provincia</option>
                                        @foreach($provincias as $provincia)
                                        <option value="{{ $provincia->id }}">{{ $provincia->nome }} - {{ $provincia->codigo }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 municipio">
                                <div class="form-group">
                                    <label>Município</label>
                                    <select name="municipio_id" class="form-control select-municipio">
                                        <option selected value="">Selecione uma opção</option>
             
                                    </select>
                                </div>
                            </div>

                            <div class="col-md-2 comuna">
                                <div class="form-group">
                                    <label>Comuna</label>
                                    <select name="comuna_id" class="form-control">
                                        <option selected value="">Selecione uma opção</option>
                                        
                                    </select>
                                </div>
                            </div>

                            @elseif(Auth::user()->nivel=='Admin' )
                            <div class="col-md-2">
                                <div class="form-group ">
                                    <label>Escolhe uma opção</label>
                                    <input type="text" readonly value="{{ Auth::user()->estacao->municipio->provincia->codigo }} - {{ Auth::user()->estacao->nome }} ({{ Auth::user()->estacao->tipo }})" class="form-control">
                                    <input type="hidden" name="provincia_origem[]" value="{{ Auth::user()->estacao->municipio->provincia->id }}">

                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group ">
                                    <label>Município</label>
                                    <select name="municipio_id" class="form-control select-municipio">
                                        @foreach($municipios as $municipio)
                                        <option value="{{ $municipio->id }}">{{ $municipio->nome }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                            @else
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>Comuna</label>
                                    <input type="text" readonly value="{{ Auth::user()->estacao->municipio->provincia->codigo }} - {{ Auth::user()->estacao->nome }} ({{ Auth::user()->estacao->tipo }})" class="form-control">
                                    <input type="hidden" name="comuna_id" value="{{ Auth::user()->estacao->id }}">
                                </div>
                            </div>
                            @endif

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Início</label>
                                    <input type="date" name="data_inicio" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Término</label>
                                    <input type="date" name="data_fim" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <div id="bo">
                                        <button type="submit" class="btn btn-primary form-control" style="margin-top:25px"> <i class="fa fa-print"></i> Imprimir</button>
                                    </div>
                                </div>
                            </div>
                        </form>


                        <!-- /.box-header -->
                        <div class="box-body">
                            <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>Emissor</th>
                                                    <th>Receptor</th>
                                                    <th>Serie</th>
                                                    <th>Estação de Origem</th>
                                                    <th>Estação de Destino</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php $a = 1; ?>
                                                @foreach($registos as $registo)
                                                <tr>
                                                    <td>{{ $a }}</td>
                                                    <td>{{ $registo->envia }}</td>
                                                    <td>{{ $registo->recebe }}</td>
                                                    <td>{{ $registo->numero_serie }}</td>
                                                    <td>{{ $registo->usuario->estacao->municipio->provincia->codigo }} - {{ $registo->usuario->estacao->nome }} ({{ $registo->usuario->estacao->tipo }})</td>
                                                    <td>{{ $registo->estacao->municipio->provincia->codigo }} - {{ $registo->estacao->nome }} ({{ $registo->estacao->tipo }})</td>

                                                </tr>
                                                <?php $a++; ?>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>

</div>
@endsection
@section('js')




<script src="{{ URL::asset('plugins/jquery/jquery.min.js') }}"></script>

<script src="{{ URL::asset('plugins/select2/js/select2.full.min.js') }}"></script>

<script>
    $(".provincia_multiple").hide();

    $("[name=all]").on("change", function(e) {

        if ($(this).prop("checked") == true) {
            $(".provincia_multiple").fadeIn(200);

            $(".comuna").hide();
            $(".municipio").hide();
            $(".provincia").hide();


        } else {
            $(".provincia_multiple").hide();

            $(".comuna").fadeIn(200);
            $(".municipio").fadeIn(200);
            $(".provincia").fadeIn(200);

        }

    });


    $(document).on('change', '.select-municipio', function() {
        $("[name=comuna_id]").html('<option selected disabled value="">Selecione um Comuna</option>');
        let muni_id = $(this).val();
        $.ajax({
            'async': false,
            'type': "GET",
            'global': false,
            'dataType': 'json',
            'url': "/public/comuna/" + muni_id,
            'data': {
                'request': "",
                'target': 'arrange_url',
                'method': 'method_target'
            },
            'success': function(data) {
                //console.log(data);
                for (var i = 0; i < data.length; i++) {
                    $("[name=comuna_id]").append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                }
            }
        });
    });



    $(document).on('change', '.select-provincia', function() {
        $("[name=municipio_id]").html('<option selected disabled value="">Selecione um município</option>');
        let prov_id = $(this).val();
        $.ajax({
            'async': false,
            'type': "GET",
            'global': false,
            'dataType': 'json',
            'url': "/public/municipios/" + prov_id,
            'data': {
                'request': "",
                'target': 'arrange_url',
                'method': 'method_target'
            },
            'success': function(data) {
                //console.log(data);
                for (var i = 0; i < data.length; i++) {
                    $("[name=municipio_id]").append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
                }
            }
        });
    });

    $(function() {
        //Initialize Select2 Elements
        $('.select2').select2({
            theme: 'bootstrap4',
            placeholder: 'Selecione pelo menos uma província'
        })

    })
</script>
<script type="text/javascript">
    $("[name=data_fim]").attr({
        "min": $("[name=data_inicio]").val()
    });
    $("[name=data_fim]").val($("[name=data_inicio]").val());


    $("[name=data_inicio]").on("change", function(e) {

        $("[name=data_fim]").val($(this).val());
        $("[name=data_fim]").attr({
            "min": $(this).val()
        });

    });

    $("[name=mes]").attr("disabled", "disabled");
    $("[name=ano]").attr("disabled", "disabled");

    $("#mes_ano_toogle").on("click", function(e) {
        if ($("#mes_ano_toogle").is(":checked")) {

            $("[name=mes]").removeAttr("disabled");
            $("[name=ano]").removeAttr("disabled");

        } else {

            $("[name=mes]").attr("disabled", "disabled");
            $("[name=ano]").attr("disabled", "disabled");

        }

    });
</script>
@endsection
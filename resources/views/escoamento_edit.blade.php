@extends('main')

@section('css')
    
  <link rel="stylesheet" href="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

@endsection

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SGT
      <small>Sistema de Gestão de Tráfego</small>
    </h1>
    <ol class="breadcrumb">
    
    <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Ocorrência</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Ocorrência</h3>
            
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action="{{ route('escoamento.update', $escoamento->id) }}" method="POST" role="form">
          @method('PUT')
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            <div class="box-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

           


           
               <div class="col-md-4">
                <div class="form-group">
                  <label>Tipo Trafego</label>
                  <select name="tipo_trafego_id" id="" class="form-control" required> 
                  <option value="">Seleciona Trafego </option>
                  @foreach( $tipoTrafego as $tf)
                      <option @if($tf->id == $escoamento->id_tipo_trafego) selected @endif value="{{ $tf->id }}">{{ $tf->descricao}} </option>
                    @endforeach
                  </select>
                </div>
               </div>

               <div class="col-md-4">
                <div class="form-group">
                  <label>Grau seguranca</label>
                  <select name="grau_seguranca_id" id="" class="form-control" required>
                  <option value="">Seleciona Grau de Seguranca </option>
                  @foreach( $grauSeguranca as $gs)
                      <option @if($gs->id == $escoamento->id_grau_seguranca) selected @endif value="{{ $gs->id }}">{{ $gs->descricao}} </option>
                    @endforeach
                  </select>  

                </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                  <label>Grau Precedencia</label>
                  <select name="grau_precedencia_id" id="" class="form-control" required>
                  <option value="">Seleciona Grau de Precedencia </option>
                  @foreach( $grauPrecedencia as $gp)
                      <option @if($gp->id == $escoamento->id_grau_precedencia) selected @endif value="{{ $gp->id }}">{{ $gp->descricao}} </option>
                    @endforeach
                  </select>  

                </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                  <label>Estação de Origem</label>
                  <input type="text" readonly value="{{ Auth::user()->estacao->codigo }} - {{ Auth::user()->estacao->nome }}" class="form-control">             
                </div>
               </div>


               <div class="col-md-4">
                <div class="form-group">
                  <label>Estação de Destino</label>
                  <select name="id_estacao_trabalho_recebe"  id="" class="form-control chosen-select" required>
                  <option value="">Seleciona Estação de Origem</option>
                    @foreach( $estacoes as $estacao)
                      @if($estacao->id != Auth::user()->estacao->id)
                        <option @if($estacao->id == $escoamento->id_estacao_trabalho_recebe) selected @endif value="{{ $estacao->id }}">{{ $estacao->nome}} </option>
                      @endif
                    @endforeach
                  </select>                  
                </div>
               </div>

               <div class="col-md-4">
                <div class="form-group">
                  <label>De.:</label>
                  <input type="text" name="envia" class="form-control" placeholder="Emissor" required value="{{ $escoamento->envia }}">
                </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                  <label>Para.:</label>
                  <input type="text" name="recebe" class="form-control" placeholder="Receptor" required value="{{ $escoamento->recebe }}">
                </div>
               </div>
               <div class="col-md-2">
                <div class="form-group">
                  <label>Origem.:</label>
                  <input type="text" name="origem" class="form-control" required value="{{ $escoamento->origem }}">                                    
                </div>
               </div>
 

               <div class="col-md-2">
                <div class="form-group">
                  <label>Série.:</label>
                  <input type="number"  name="numero_serie"  class="form-control" required value="{{ $escoamento->numero_serie }}">
                 
                </div>
               </div>

               <div class="col-md-12">
                <div class="form-group">
           
           <textarea class="textarea1" name="mensagem" 
           style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" required  >{{ $escoamento->mensagem }}</textarea>
         
       </div>
          </div>

               </div>

                </div>
               </div>

            
            </div>
            <!-- /.box-body -->

            <div class="box-footer text-right">
              <button type="submit" class="btn btn-primary">Actualizar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
@endsection

@section('js')
    
<!-- Bootstrap WYSIHTML5 -->
<script src="/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script>
  $(function () {
    //bootstrap WYSIHTML5 - text editor
    $('.textarea').wysihtml5()
  })
</script>


@endsection
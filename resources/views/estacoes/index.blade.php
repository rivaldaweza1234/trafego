@extends('main')

@section('css')

<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

@endsection

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SGT
            <small>Sistema de Gestão de Tráfego</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Estações</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger">
                        {!! session('error') !!}
                    </div>
                @endif
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Estações</h3>
                        <div class="text-right" style="margin-top: -25px;">
                            <a href="{{ route('estacao.create') }}" class="btn btn-primary">
                                <i class="fa fa-plus-circle"></i>
                                Nova Estação
                            </a>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Nome</th>
                                                <th>Tipo</th>
                                                <th>Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $a = 1; ?>
                                           
                                            @foreach($estacoes as $estacao)
                                           
                                                <tr>
                                                    <td>{{ $a }}</td>
                                                    <td>{{ $estacao->nome }} - {{ $estacao->municipio->provincia->codigo }}</td>
                                                    <td>{{ $estacao->tipo }}</td>
                                                    <td class="text-center">
                                                        <form action="{{ route('estacao.destroy', $estacao->id) }}" method="POST">
                                                            @method('DELETE')
                                                            <a href="{{ route('estacao.edit', $estacao->id) }}" class="btn btn-sm btn-primary">Editar</a>
                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                            <button class="btn btn-sm btn-danger">Excluir</button>
                                                            <a href="{{ route('toggle.state', $estacao->id) }}" class="btn btn-sm btn-warning">{{ ($estacao->estado == "Activa") ? "Desactivar" : "Activar"}}</a>
                                                        </form>
                                                    </td>
                                                </tr>
                                               
                                            
                                            <?php $a++; ?>
                                          

                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
@endsection
@section('js')

<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- page script -->
<script>
    $(function() {
        $('#example1').DataTable()
    })
</script>

@endsection
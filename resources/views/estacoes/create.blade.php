@extends('main')

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SGT
      <small>Sistema de Gestão de Tráfego</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Estações</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Nova Estação</h3>
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form role="form" action="{{ route('estacao.store') }} " method="POST">
            <div class="box-body">

              <div class="col-md-3">
                <div class="form-group">
                  <label>Nome</label>
                  <input type="text" name="nome" class="form-control" autocomplete="off" required>
                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                </div>
              </div>

              @if(Auth::user()->nivel=='Admin')

              <div class="col-md-3">
                <div class="form-group">
                  <label>Tipo</label>
                  <select name="tipo" class="form-control" required>
                    <option selected disabled value="">Selecione tipo de Estação</option>
                    <option value="Municipal">Municipal</option>
                    <option value="Comunal">Comunal</option>
                  </select>
                </div>
              </div>

              @else()

              <div class="col-md-3">
                <div class="form-group">
                  <label>Tipo</label>
                  <select name="tipo" class="form-control" required>
                    <option selected disabled value="">Selecione tipo de Estação</option>
                    <option value="Sede">Sede</option>
                    <option value="Municipal">Municipal</option>
                    <option value="Comunal">Comunal</option>
                  </select>
                </div>
              </div>

              @endif


              @if(Auth::user()->nivel=='Admin' )

              <div class="col-md-3">
                <div class="form-group">
                  <label>Província</label>
                  <input type="text" readonly value="{{ Auth::user()->estacao->municipio->provincia->nome }} - {{ Auth::user()->estacao->municipio->provincia->codigo }}" class="form-control">
                </div>
              </div>

              @else

              <div class="col-md-3">
                <div class="form-group">
                  <label>Província</label>
                  <select name="provincia_id" class="form-control select-provincia" required>
                    <option selected disabled value="">Selecione uma província</option>
                    @foreach($provincias as $provincia)
                    <option value="{{ $provincia->id }}">{{ $provincia->nome }} - {{ $provincia->codigo }}</option>
                    @endforeach
                  </select>
                </div>
              </div>

              @endif

              <div class="col-md-3">
                <div class="form-group">
                  <label>Município</label>
                  <select name="municipio_id" class="form-control" required>
                    @foreach($municipios as $municipio)
                      <option value="{{ $municipio->id }}">{{ $municipio->nome }}</option>
                    @endforeach
                  </select>
                </div>
              </div> 

            </div>
            <!-- /.box-body -->

            <div class="box-footer text-right">
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
@endsection
@section('js')
<script type="text/javascript">

$(document).on('change', '.select-provincia', function() {
  $("[name=municipio_id]").html('<option selected disabled value="">Selecione um município</option>');
  let prov_id = $(this).val();
  $.ajax({
        'async': false,
        'type': "GET",
        'global': false,
        'dataType': 'json',
        'url': "/public/municipios/"+prov_id,
        'data': {
            'request': "",
            'target': 'arrange_url',
            'method': 'method_target'
        },
        'success': function(data) {
            //console.log(data);
            for (var i = 0; i < data.length; i++) {
                $("[name=municipio_id]").append('<option value="' + data[i].id + '">' + data[i].nome + '</option>');
            }
        }
    });
});

</script>
@endsection
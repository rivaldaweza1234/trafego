@extends('main')

@section('css')
    
  <link rel="stylesheet" href="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

 
  @endsection

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      SGT
      <small>Sistema de Gestão de Tráfego</small>
    </h1>
    <ol class="breadcrumb">
    <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Ocorrência</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <!-- Main row -->
    <div class="row">
      <div class="col-md-12">
        <div class="box box-primary">
          <div class="box-header with-border">
            <h3 class="box-title">Ocorrência</h3>
            
          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action="{{ route('ocorrencia.store') }}" method="POST" role="form">
          @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
            <div class="box-body">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="col-md-4">
                <div class="form-group">
                  <label>Tipo Ocorrencia</label>
                  <select name="tipo_ocorrencia_id" id="" class="form-control" required>
                  <option value="">Seleciona o Tipo de Ocorrencia</option>
                    @foreach( $ocorrencia as $oc)
                      <option value="{{ $oc->id }}">{{ $oc->descricao}} </option>
                    @endforeach
                  </select>  

                </div>
               </div>
               <br>
           
            
               <div class="col-md-12">
                <div class="form-group">
           
           <textarea class="" name="mensagem" 
           style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
         
       </div>
       <div class="box-footer text-right">
              <button type="submit" class="btn btn-primary">Salvar</button>
            </div>
          </div>
            <!-- /.box-body -->

           
          </form>
        </div>
      </div>

      
    </div>
    <!-- /.row (main row) -->

  </section>
  <!-- /.content -->
</div>
@endsection

@section('js')
    
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
 

 


@endsection
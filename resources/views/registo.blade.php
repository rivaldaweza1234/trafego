 @extends('main')

 @section('main-content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
     <h1>
        SGT
       <small>Sistema de Gestão de Tráfego</small>
     </h1>
     <ol class="breadcrumb">
     <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active">Aceitação e Registo de Tráfego</li>
         
     </ol>
   </section>




   <!-- Main content -->
   <section class="content">
     <!-- Main row -->
     <style>

     
     </style>
     <div class="row">
       <div class="col-md-12">
  
                <div class="form-group" style="position:relative;margin-right:50px">
                   
         <div class="box box-primary">
           <div class="box-header with-border">
             <h3 class="box-title">Aceitação e Registo</h3>
           
            

               </div>
           </div>
           
           <!-- /.box-header -->
           <!-- form start -->
           <form action="{{ route('registo.store') }}" method="POST" role="form" >
           @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @elseif(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
             <div class="box-body">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <input type="hidden"> 

               <div class="col-md-4">
                <div class="form-group">
                  <label>Tipo Operação</label>
                  <select name="tipo_operacao_id" id="" class="form-control" required> 
                    <option value="">Seleciona Operação </option>
                    @foreach( $operacao as $op)
                      <option value="{{ $op->id }}">{{ $op->descricao}} </option>
                    @endforeach
                  </select>
                </div>
               </div>

               <div class="col-md-4">
                <div class="form-group">
                  <label>Tipo Trafego</label>
                  <select name="tipo_trafego_id" id="" class="form-control" required> 
                  <option value="">Seleciona Trafego </option>
                    @foreach( $trafego as $tf)
                      <option value="{{ $tf->id }}">{{ $tf->descricao}} </option>
                    @endforeach
                  </select>
                </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                  <label>Grau seguranca</label>
                  <select name="grau_seguranca_id" id="" class="form-control" required>
                  <option value="">Seleciona Grau de Seguranca </option>
                    @foreach( $seguranca as $gs)
                      <option value="{{ $gs->id }}">{{ $gs->descricao}} </option>
                    @endforeach
                  </select>  

                </div>
               </div>

               <div class="col-md-4">
                <div class="form-group">
                  <label>Grau Precedencia</label>
                  <select name="grau_precedencia_id" id="" class="form-control" required>
                  <option value="">Seleciona Grau de Precedencia </option>
                    @foreach( $grau as $gp)
                      <option value="{{ $gp->id }}">{{ $gp->descricao}} </option>
                    @endforeach
                  </select>  

                </div>
               </div>
               <div class="col-md-4">
                <div class="form-group">
                  <label>Estação de Origem</label>
                  <input type="text" readonly value="{{ Auth::user()->estacao->codigo }} - {{ Auth::user()->estacao->nome }}" class="form-control">             
                </div>
               </div>

                  


               <div class="col-md-4">
                <div class="form-group">
                  <label>Estação de Destino</label>
                  <select name="id_estacao_trabalho_recebe"  id="" class="form-control chosen-select" required>
                  <option value="">Seleciona Estação de Origem</option>
                    @foreach( $estacoes as $estacao)
                        @if($estacao->id != Auth::user()->estacao->id)
                        <option value="{{ $estacao->id }}" >{{$estacao->codigo}} - {{$estacao->nome}}</option>
                        @endif
                    @endforeach
                  </select>                  
                </div>
               </div>

               <div class="col-md-4">
                <div class="form-group">
                  <label>De.:</label>
                  <input type="text" name="envia" class="form-control" placeholder="Emissor" required>
                </div>
               </div>

               <div class="col-md-4">
                <div class="form-group">
                  <label>Para.:</label>
                  <input type="text" name="recebe" class="form-control" placeholder="Receptor" required>
                </div>
               </div>
 


               <div class="col-md-2">
                <div class="form-group">
                  <label>Origem.:</label>
                  <input type="number" class="form-control" name="origem" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  maxlength="4" required>
                                                 
                </div>
               </div>

               

               <div class="col-md-2">
                <div class="form-group">
                  <label>Ano.:</label>
                  <input type="number" class="form-control" name="ano" onkeypress="return event.charCode >= 48 && event.charCode <= 57"  maxlength="4" required>

                </div>
               </div>

               <div class="col-md-2">
                <div class="form-group">
                  <label>Série.:</label>
                  <input type="text"  name="serie"  class="form-control" onkeydown="javascript: fMasc( this, mCPF );"maxlength="8">
                
                </div>
               </div>
               </div>

              
               </div>

              

             </div>
             <!-- /.box-body -->

             <div class="box-footer text-right">
               <button type="submit" class="btn btn-primary">Salvar</button>
             </div>
           </form>

         </div>
       </div>
     </div>
     <!-- /.row (main row) -->

   </section>
   <!-- /.content -->
 </div>
 @endsection
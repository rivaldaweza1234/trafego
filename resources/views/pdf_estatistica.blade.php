<?php error_reporting(E_ALL ^ E_DEPRECATED); ?>
<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Relarório</title>

    <style>
        body {
            font-family: arial, sans-serif;
        }

        table {
            font-family: arial, sans-serif;
            border-collapse: collapse;
            width: 100%;
        }

        td,
        th {
            border: 1px solid #dddddd;
            text-align: left;
            padding: 8px;
        }

        tr:nth-child(even) {
            background-color: #dddddd;
        }
    </style>
</head>

<body>

    @if(isset($ocorrencias))
    <h2>Dados Filtrados - Ocorrências</h2>

    <table>
        <tr>
            <th>#</th>
            <th>Tipo de Ocorrência</th>
            <th>Utilizador</th>
            <th>Data</th>
        </tr>
        <?php $a = 1; ?>
        @foreach($ocorrencias as $key => $ocorrencia)
        <tr>
            <td>{{ $a }}</td>
            <td>{{ $ocorrencia->tipo_ocorrencia->descricao }}</td>
            <td>{{ $ocorrencia->usuario->nome }}</td>
            <td>{{ $ocorrencia->created_at }}</td>
        </tr>
        @endforeach

    </table>

    <!-- @elseif(isset($estacao))

    <div style="text-align: center;">
        <h4>REPÚBLICA DE ANGOLA</h4>
        <h4>MINISTÉRIO DAS TELECOMUNICAÇÕES E TECNOLOGIAS DE INFORMAÇÃO</h4>
        <h4>INSTITUTO NACIONAL DE FOMENTO DA SOCIEDADE DE INFORMAÇÃO</h4>
        <h4>ÁREA DE ESCOAMENTE DE TRÁFEGO</h4>  
        <h4>comuna</h4>  
        <br>
    </div>
    <table>
        <thead style="text-transform: uppercase;">
            <tr>
                <th>INFOSI</th>
                <th style="text-align: center;" colspan="6">RESUMO DO TRÁFEGO </th>

            </tr>
            <tr>
                <td rowspan="2">Estação</td>
                <td colspan="2">Expedidos</td>
                <td colspan="2">Recebidos</td>

                <td colspan="2">Total</td>
            </tr>
            <tr>
                <td>MSG.</td>
                <td>GR</td>
                <td>MSG</td>
                <td>GR</td>

                <td>MSG</td>
                <td>GR</td>
            </tr>
        </thead>
        <tbody>

            <tr>
                
                <td>{{ $estacao->nome }}</td>
                <td>{{ getBy('expedido', $estacao->id) }}</td>
                <td>0</td>
                <td>{{ getBy('recebido', $estacao->id) }}</td>
                <td>0</td>
                <td>0</td>
                <td>0</td>

            </tr>



        </tbody>
    </table>

-->
    @else(isset($est))

<div style="text-align: center;">
    <h4>REPÚBLICA DE ANGOLA</h4>
    <h4>MINISTÉRIO DAS TELECOMUNICAÇÕES E TECNOLOGIAS DE INFORMAÇÃO</h4>
    <h4>INSTITUTO NACIONAL DE FOMENTO DA SOCIEDADE DE INFORMAÇÃO</h4>
    <h4>ÁREA DE ESCOAMENTE DE TRÁFEGO</h4>  
    <h4>Estação</h4>  
    <br>
</div>
<table>
    <thead style="text-transform: uppercase;">
        <tr>
            <th>INFOSI</th>
            <th style="text-align: center;" colspan="6">RESUMO DO TRÁFEGO </th>

        </tr>
        <tr>
            <td rowspan="2">Estação</td>
            <td colspan="2">Expedidos</td>
            <td colspan="2">Recebidos</td>

            <td colspan="2">Total</td>
        </tr>
        <tr>
            <td>MSG.</td>
            <td>GR</td>
            <td>MSG</td>
            <td>GR</td>

            <td>MSG</td>
            <td>GR</td>
        </tr>
    </thead>
    <tbody>

        <tr> 
        </tr>



    </tbody>
</table>




    <!-- @elseif(isset($municipio))

<div style="text-align: center;">
    <h4>REPÚBLICA DE ANGOLA</h4>
    <h4>MINISTÉRIO DAS TELECOMUNICAÇÕES E TECNOLOGIAS DE INFORMAÇÃO</h4>
    <h4>INSTITUTO NACIONAL DE FOMENTO DA SOCIEDADE DE INFORMAÇÃO</h4>
    <h4>ÁREA DE ESCOAMENTE DE TRÁFEGO</h4>
    <h4>Municipio</h4>
    <br>
</div>
<table>
        <thead style="text-transform: uppercase;">
            <tr>
                <th>INFOSI</th>
                <th style="text-align: center;" colspan="6">RESUMO DO TRÁFEGO </th>

            </tr>
            <tr>
                <td rowspan="2">Estação</td>
                <td colspan="2">Expedidos</td>
                <td colspan="2">Recebidos</td>

                <td colspan="2">Total</td>
            </tr>
            <tr>
                <td>MSG.</td>
                <td>GR</td>
                <td>MSG</td>
                <td>GR</td>

                <td>MSG</td>
                <td>GR</td>
            </tr>
        </thead>
        <tbody>

        <tr>
                
                <td>{{ $municipio->nome }}</td>
                <td>{{ get_data_by_municipio('expedido', $municipio->id) }}</td>
                <td>0</td>
                <td>{{ get_data_by_municipio('recebido', $municipio->id) }}</td>
                <td>0</td>
                <td>{{ get_data_by_municipio('expedido', $municipio->id) + get_data_by_municipio('recebido', $municipio->id) }}</td>
                <td>0</td>

            </tr>



        </tbody>
    </table>
    -->
 <!--
 @else(isset($provincias))
    <div style="text-align: center;">
        <h4>REPÚBLICA DE ANGOLA</h4>
        <h4>MINISTÉRIO DAS TELECOMUNICAÇÕES E TECNOLOGIAS DE INFORMAÇÃO</h4>
        <h4>INSTITUTO NACIONAL DE FOMENTO DA SOCIEDADE DE INFORMAÇÃO</h4>
        <h4>ÁREA DE ESCOAMENTE DE TRÁFEGO</h4>
        <h4>provincia</h4>
        <br>
    </div>
    <table>
        <thead style="text-transform: uppercase;">
            <tr>
                <th>INFOSI</th>
                <th style="text-align: center;" colspan="6">RESUMO GLOBAL DO TRÁFEGO</th>
                <th colspan="2">MÊS DE {{$month}} / {{$year}}</th>
            </tr>
            <tr>
                <td rowspan="2">PROVÍNCIA</td>
                <td colspan="2">Sede</td>
                <td colspan="2">Pst. Municipais</td>
                <td colspan="2">Pst. Comunais</td>
                <td colspan="2">Total</td>
            </tr>
            <tr>
                <td>Mens.</td>
                <td>Palavras</td>
                <td>Mens.</td>
                <td>Palavras</td>
                <td>Mens.</td>
                <td>Palavras</td>
                <td>Mens.</td>
                <td>Palavras</td>
            </tr>
        </thead>
        <tbody>
            @foreach($provincias as $provincia)
            <tr>
                <td>{{ $provincia->nome }} - Sede </td>
                <td>{{ get_data_by_provincia($provincia->id, 'Sede') }} </td>
                <td>0</td>
                <td>{{ get_data_by_provincia($provincia->id, 'Municipal') }}</td>
                
                <td>0</td>
                <td>{{ get_data_by_provincia($provincia->id, 'Comunal') }}</td>
                <td>0</td>
                <td>{{ get_data_by_provincia($provincia->id) }}</td>
                <td>0</td>
            </tr>
            @endforeach


        </tbody>
    </table>
-->
    @endif

</body>

</html>
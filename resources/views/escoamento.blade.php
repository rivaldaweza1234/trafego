 @extends('main')
 @section('css')

 <link rel="stylesheet" href="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">

 <link rel="stylesheet" href="{{ URL::asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
 <link rel="stylesheet" href="{{ URL::asset('plugins/select2/css/select2.min.css') }}">

 @section('main-content')
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
   <!-- Content Header (Page header) -->
   <section class="content-header">
     <h1>
       SGT
       <small>Sistema de Gestão de Tráfego</small>
     </h1>
     <ol class="breadcrumb">
       <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
       <li class="active">Escoamento de Tráfego</li>
     </ol>
   </section>

   <!-- Main content -->
   <section class="content">
     <!-- Main row -->
     <div class="row">
       <div class="col-md-12">
         <div class="box box-primary">
           <div class="box-header with-border">
             <h3 class="box-title">Expedir</h3>

           </div>

           <!-- /.box-header -->
           <!-- form start -->
           <form action="{{ route('escoamento.store') }}" method="POST" role="form" enctype="multipart/form-data">
             @csrf()
             @if (session('success'))
             <div class="alert alert-success">
               {{ session('success') }}
             </div>
             @elseif(session('error'))
             <div class="alert alert-danger">
               {{ session('error') }}
             </div>
             @endif
             <div class="box-body">
               <input type="hidden" name="_token" value="{{ csrf_token() }}">

               <input type="hidden">



               <div class="col-md-4">
                 <div class="form-group">
                   <label>Tipo Trafego</label>
                   <select name="tipo_trafego_id" id="" class="form-control" required>
                     <option value="">Seleciona Trafego </option>
                     @foreach( $trafego as $tf)
                     <option value="{{ $tf->id }}">{{ $tf->descricao}} </option>
                     @endforeach
                   </select>
                 </div>
               </div>
               <div class="col-md-4">
                 <div class="form-group">
                   <label>Grau seguranca</label>
                   <select name="grau_seguranca_id" id="classificacao" class="form-control" required>
                     <option value="">Seleciona Grau de Seguranca </option>
                     @foreach( $seguranca as $gs)
                     <option value="{{ $gs->id }}">{{ $gs->descricao}} </option>
                     @endforeach
                   </select>

                 </div>
               </div>

               <div class="col-md-4">
                 <div class="form-group">
                   <label>Grau Precedencia</label>
                   <select name="grau_precedencia_id" id="grau_precedencia" class="form-control" required>
                     <option value="">Seleciona Grau de Precedencia </option>
                     @foreach( $grau as $gp)
                     <option value="{{ $gp->id }}">{{ $gp->descricao}} </option>
                     @endforeach
                   </select>

                 </div>
               </div>

               <div class="col-md-4">
                 <div class="form-group">
                   <label>Estação de Origem</label>
                   <input type="text" readonly value="{{ Auth::user()->estacao->municipio->provincia->codigo }} - {{ Auth::user()->estacao->nome }} ({{ Auth::user()->estacao->tipo }})" class="form-control">
                 </div>
               </div>
               

               <div class="col-md-4">
                <div class="form-group">
                  <label>Estação de Destino</label>
                  <select name="id_estacao_trabalho_recebe[]"  class="select2" multiple="multiple" id="" class="form-control "   style="width: 100%;">required>
                    @foreach( $estacoes as $estacao)
                    
                        @if($estacao->id != Auth::user()->estacao->id)
                    
                        <option value="{{ $estacao->id }}" >{{$estacao->municipio->provincia->codigo}} - {{$estacao->nome}} ({{$estacao->tipo}})</option>
                        @endif
                    @endforeach

                  </select>                  
                </div>
               </div>

               <div class="col-md-4">
                 <div class="form-group">
                   <label>De.:</label>
                   <input type="text" name="envia" id="de" class="form-control" placeholder="Emissor" required>
                 </div>
               </div>

               <div class="col-md-4">
                 <div class="form-group">
                   <label>Para.:</label>
                   <input type="text" name="recebe" id="para" class="form-control" placeholder="Receptor" required>
                 </div>
               </div>
               <div class="col-md-2">
                 <div class="form-group">
                   <label>Serie.:</label>
                   <input type="number" name="numero_serie" id="serie" class="form-control" maxlength="12" required>

                 </div>
               </div>


               <div class="col-md-2">
                 <div class="form-group">
                   <label>Origem.:</label>
                   <input type="text" name="origem"id="origem" class="form-control" maxlength="12" required>

                 </div>

               </div>



               <div class="col-md-3">
                 <div class="form-group">
                   <label>Anexo.:</label>

                   <input type="file" name="anexo" class="form-control">

                 </div>
               </div>


               <div class="col-md-1">
                 <div class="form-group">
                   <label>Mês.:</label>
                   <input type="text" name="mes" class="form-control" readonly value="{{ date('m') }}">

                 </div>
               </div>

               <div class="col-md-12">
                 <div class="form-group">
               
              <textarea class="" name="mensagem" id="msga" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>

                    </div>
               </div>

               

             </div>
             <div class="box-footer text-right">
             
               <button type="button" class="btn btn-primary real-buttom" data-toggle="modal" data-target="#modalConfirm" onclick="myFunction()">Confirmar os dados</button>
            
               <div id="modalConfirm" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title"></h4>
       </div>
       <div class="modal-body">
         

       <table cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:none" center>
	<tbody>
         
    <div id="divResultado">
    </div>
  <center>   <h3>  Confirme os dados se estão corretos   </h3> </center>
   
       <div>
    Grupo de palavras 
     <p id="demo"> </p>   
     </div>
    
     

        
			<td colspan="8" style="border-bottom:1px solid black;  solid black;  solid black; border-top:none; height:16px; vertical-align:top; width:564px">
			 
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:16px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:37px vertical-align:top; width:457px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Grau de precedencia <label id="show_grau_precedencia"></label>  <center>   </center>  </span></span></p>
			</td>
			<td colspan="4" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:16px; vertical-align:top; width:213px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><center>Data e Hora</center><center>  </center></span></span></p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:16px; vertical-align:top; width:225px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Serie:  <label id="show_serie"></label> </span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:16px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" rowspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:8px; vertical-align:top; width:339px">
			<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp De:<label id="show_de"></label> </span></span></p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:8px; vertical-align:top; width:225px">
			<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Clasificação <label id="show_classificacao"></label></span></span></p>
			</td>
			 
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:8px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" rowspan="2" style="border-bottom:none; border-left:none; border-right:1px solid black; border-top:none; height:18px; vertical-align:top; width:225px">
      &nbsp 	Numero origem : <label id="show_origem"></label>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:18px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" rowspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:18px; vertical-align:top; width:339px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Para:  <label id="show_para"></label> </span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:18px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:13px; vertical-align:top; width:225px">
			 	</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:13px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:31px; vertical-align:top; width:564px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> </span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:31px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:60px">
			<p>&nbsp;</p>
			</td>
			<td colspan="3" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:87px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Data</span></span></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:93px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Hora</span></span></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:99px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Operador  </span></span></p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:225px">
			<p>&nbsp;</p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:25px; width:0px">&nbsp;</td>
		</tr>
		 
	 
	</tbody>
</table>



       </div>
       <div class="modal-footer">
       <center>    <h4 style="float:left">    OBS:    </h4>   <h4 style="color:red;float:left"> Se Salvar os dados ja não podera fazer alterações  </h4> </center>
       <button type="submit" class="btn btn-primary real-buttom">Expedir</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Editar</button>
       </div>
     </div>

   </div>
 </div>





              </div>
             <!-- /.col-->
         </div>
         <!-- ./row -->



       </div>
       <!-- /.box-body -->


       </form>
     </div>
 </div>
 </div>
       
<script> 
 
 function myFunction(){
            var de = document.getElementById("de").value;
            var para = document.getElementById("para").value;
            var serie = document.getElementById("serie").value;
            var origem = document.getElementById("origem").value;
            var grau_precedencia = document.getElementById("grau_precedencia").value;
            var classificacao = document.getElementById("classificacao").value;
            
            var div = document.getElementById("divResultado");
            
            document.getElementById("show_de").innerText = de; 
            document.getElementById("show_para").innerText = para;
            document.getElementById("show_serie").innerText = serie;
            // document.getElementById("show_grau_precedencia").innerText = grau_precedencia;
            document.getElementById("show_origem").innerText = origem;
            //document.getElementById("show_classificacao").innerText = classificacao;

             
        }

        
$("#msga").on('keyup', function(e){
      var str = $(this).val();
      if(str){
        var res = str.split(" ");
        $("#demo").html(res.length);
      }else{
        $("#demo").html(1);

      }
});

</script>
 
 <!-- /.content -->
 </div>
 @endsection
 @section('modal')
 <!-- Modal -->









 <div id="modalConfirm" class="modal fade" role="dialog">
   <div class="modal-dialog modal-lg">

     <!-- Modal content-->
     <div class="modal-content">
       <div class="modal-header">
         <button type="button" class="close" data-dismiss="modal">&times;</button>
         <h4 class="modal-title"></h4>
       </div>
       <div class="modal-body">
         

       <table cellspacing="0" class="MsoTableGrid" style="border-collapse:collapse; border:none" center>
	<tbody>
        
  <center>   <h3>  Confirme os dados se estão corretos   </h3> </center>
   
       <div>
    Grupo de palavras 
     <p id="demo"> </p>   
     </div>
      
        
			<td colspan="8" style="border-bottom:1px solid black;  solid black;  solid black; border-top:none; height:16px; vertical-align:top; width:564px">
			 
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:16px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:37px vertical-align:top; width:457px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Grau de precedencia  <center>   </center>  </span></span></p>
			</td>
			<td colspan="4" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:16px; vertical-align:top; width:213px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"><center>Data e Hora</center><center>  </center></span></span></p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:16px; vertical-align:top; width:225px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Serie: </span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:16px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" rowspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:8px; vertical-align:top; width:339px">
			<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp De:  </span></span></p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:8px; vertical-align:top; width:225px">
			<p style="text-align:justify"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Clasificação</span></span></p>
			</td>
			 
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:8px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" rowspan="2" style="border-bottom:none; border-left:none; border-right:1px solid black; border-top:none; height:18px; vertical-align:top; width:225px">
      &nbsp 	Numero origem : 
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:18px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="6" rowspan="2" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:18px; vertical-align:top; width:339px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Para: </span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:18px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:13px; vertical-align:top; width:225px">
			 	</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:13px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="8" style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:31px; vertical-align:top; width:564px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> </span></span></p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:31px; width:0px">&nbsp;</td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid black; border-left:1px solid black; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:60px">
			<p>&nbsp;</p>
			</td>
			<td colspan="3" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:87px">
			<p><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Data</span></span></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:93px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif">Hora</span></span></p>
			</td>
			<td style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:99px">
			<p style="text-align:center"><span style="font-size:11pt"><span style="font-family:Calibri,sans-serif"> &nbsp Operador  </span></span></p>
			</td>
			<td colspan="2" style="border-bottom:1px solid black; border-left:none; border-right:1px solid black; border-top:none; height:25px; vertical-align:top; width:225px">
			<p>&nbsp;</p>
			</td>
			<td style="border-bottom:none; border-left:none; border-right:none; border-top:none; height:25px; width:0px">&nbsp;</td>
		</tr>
		 
	 
	</tbody>
</table>



       </div>
       <div class="modal-footer">
       <center>    <h4 style="float:left">    OBS:    </h4>   <h4 style="color:red;float:left"> Se Salvar os dados ja não podera fazer alterações  </h4> </center>
         <button type="submit" class="btn btn-primary real-buttom">Salvar</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Editar</button>
       </div>
     </div>

   </div>
 </div>

  











 @endsection
 @section('js')

 <script type="text/javascript">
 
$("[name=grau_seguranca_id]").on("change", function(){
  $.ajax({
            'async': false,
            'type': "GET",
            'global': false,
            'dataType': 'json',
            'url': "/public/grau-seguranca/"+$(this).val(),
            'data': {
                'request': "",
                'target': 'arrange_url',
                'method': 'method_target'
            },
            'success': function(data) {
              // console.log(data)
              $("#show_classificacao").html(data.descricao);
            }
        });
});


$("[name=grau_precedencia_id]").on("change", function(){
  $.ajax({
            'async': false,
            'type': "GET",
            'global': false,
            'dataType': 'json',
            'url': "/public/grau-precedencia/"+$(this).val(),
            'data': {
                'request': "",
                'target': 'arrange_url',
                'method': 'method_target'
            },
            'success': function(data) {
              // console.log(data)
              $("#show_grau_precedencia").html(data.descricao);
            }
        });
});

   $('form').one('submit', function(e) {
     e.preventDefault();

     // do your things ...
     $("#modalConfirm").modal();

   });

   $('.finish').on('click', function(e){
    
    $('form').submit();

   });
   // and when you done:
 </script>
 <!-- Bootstrap WYSIHTML5 -->
<script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>

<script src="{{ URL::asset('plugins/jquery/jquery.min.js') }}"></script>
 
<script src="{{ URL::asset('plugins/select2/js/select2.full.min.js') }}"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2({
      theme: 'bootstrap4',
      placeholder: 'Selecione pelo menos uma estação'
    })
    
  })
</script>

 <!-- Bootstrap WYSIHTML5 -->
 <script src="{{ URL::asset('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>

 @endsection
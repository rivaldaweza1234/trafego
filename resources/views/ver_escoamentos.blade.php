@extends('main')

@section('css')

<!-- DataTables -->
<link rel="stylesheet" href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

@endsection

@section('main-content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            SGT
            <small>Sistema de Gestão de Tráfego</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Lista Escoamento</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
                @elseif(session('error'))
                <div class="alert alert-danger">
                    {{ session('error') }}
                </div>
                @endif

                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Expedir</h3>
                        <div class="text-right" style="margin-top: -25px;">
                            <a href="{{ route('escoamento.create') }}" class="btn btn-primary">
                                <i class="fa fa-plus-circle"></i>
                                Novo
                            </a>
                        </div>
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <div id="example1_wrapper" class="dataTables_wrapper form-inline dt-bootstrap">
                            <div class="row">
                                <div class="col-lg-12">
                                    <table id="example1" class="table table-bordered table-striped dataTable" role="grid" aria-describedby="example1_info">
                                        <thead>
                                            <tr>

                                                <th>#</th>
                                                <th>Emissor</th>
                                                <th>Receptor</th>
                                                <th>Serie</th>
                                                <th>Ano</th>
                                                <th>Enviado da Estação</th>
                                                <th>Recebido da Provincia</th>
                                                <th class="text-center">Ação</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $a = 1; ?>
                                            @foreach($escoamento as $escoamento)
                                            @if($escoamento->id_usuario == Auth::user()->id)
                                            <tr>

                                                <td>{{ $a }}</td>
                                                <td>{{ $escoamento->envia }}</td>
                                                <td>{{ $escoamento->recebe }}</td>
                                                <td>{{ $escoamento->numero_serie }}</td>
                                                <td>{{ $escoamento->origem }}</td>

                                                <td> {{ $escoamento->usuario->estacao->nome }}</td>
                                                <td> {{ $escoamento->estacao->nome }}</td>


                                                <td class="text-center">
                                                    @if($escoamento->id_tipo_operacao == 1)

                                                    <a class="btn btn-sm btn-warning" href="{{ route('gerar-pdf', $escoamento->id) }}">Dar baixa</a>

                                                    @endif
                                                </td>

                                                <td>
                                                    <a class="btn btn-sm btn-primary" href="#" data-toggle="modal" data-target="#modalFullDescription{{ $escoamento->id }}">Visualizar</a>
                                                </td>
                                            </tr>

                                            <!-- Modal -->
                                            <div id="modalFullDescription{{ $escoamento->id }}" class="modal fade" role="dialog">
                                                <div class="modal-dialog">

                                                    <!-- Modal content-->
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                            <h4 class="modal-title">{{ $escoamento->id }} - {{ $escoamento->envia }}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Some text in the modal.</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            @endif
                                            <?php $a++; ?>
                                            @endforeach
                                        </tbody>

                                    </table>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
</div>
@endsection

@section('js')

<!-- DataTables -->
<script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<!-- page script -->
<script>
    $(function() {
        $('#example1').DataTable()
    })
</script>

@endsection
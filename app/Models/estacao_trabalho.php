<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\municipio;

class estacao_trabalho extends Model
{
    protected $table = 'tbl_estacao_trabalho';

    public function municipio()
    {
        return $this->belongsTo(municipio::class, 'municipio_id');
    }
    
}

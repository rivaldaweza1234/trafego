<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\provincia;

class municipio extends Model
{
    protected $table = 'tbl_municipio';
    
    public function provincia()
    {
        return $this->belongsTo(provincia::class, 'provincia_id');
    }
    
}

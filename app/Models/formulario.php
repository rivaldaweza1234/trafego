<?php

namespace App\Models; 
use App\Models\estacao_trabalho;
use App\Models\grau_precedencia; 
use App\usuarios;
use Illuminate\Database\Eloquent\Model;

class formulario extends Model
{
    protected $table = 'tbl_formulario';
 

    //estacao
    
    public function estacao()
    {
        return $this->belongsTo(estacao_trabalho::class, 'id_estacao_trabalho_recebe');
    }

    //grau_precedencia
    public function grau_precedencia()
    {
        return $this->belongsTo(grau_precedencia::class, 'id_grau_precedencia');
    }


    //usuario
    public function usuario()
    {
        return $this->belongsTo(usuarios::class, 'id_usuario');
    }


}

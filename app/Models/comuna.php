<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\municipio;

class comuna extends Model
{
    protected $table = 'tbl_comuna';

   public function municipio()
    {
        return $this->belongsTo(municipio::class, 'municipio_id');
    }


}

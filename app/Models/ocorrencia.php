<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\tipo_ocorrencia;
use App\usuarios;

class ocorrencia extends Model
{
    protected $table = 'tbl_ocorrencia';

    public function tipo_ocorrencia()
    {
        return $this->belongsTo(tipo_ocorrencia::class, 'id_tipo_ocorrencia');
    }

    public function usuario()
    {
        return $this->belongsTo(usuarios::class, 'id_usuario');
    }
}

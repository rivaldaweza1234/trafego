<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\estacao_trabalho; 

class usuarios extends Authenticatable
{
    protected $table = 'tbl_usuario';

    public function estacao()
    {
        return $this->belongsTo(estacao_trabalho::class, 'id_estacao_trabalho');
    }
    
}
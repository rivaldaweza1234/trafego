<?php

use App\Models\formulario;
use App\Models\estacao_trabalho;
 
 


function get_data_by_estacao($tipo, $estacao_id, $start = null, $end = null)
{

    $total = 0;

    if ($start && $end) {
        $registos = estacao_trabalho::whereBetween('created_at', [$start, $end])->get();
    } else {
        $registos = estacao_trabalho::all();
    }

    if ($registos) {

        foreach ($registos as $registo) {
            if ($registo->id_estacao_trabalho == $estacao_id) {
                $total++;
            }
        }
    }

    return $total;
}


function get_data_by_provincia($id, $tipo = null, $start = null, $end = null)
{

    $total = 0;

    if ($start && $end) {
        $registos = formulario::whereBetween('created_at', [$start, $end])->get();
    } else {
        $registos = formulario::all();
    }

    if ($registos) {

        foreach ($registos as $registo) {
            if ($tipo) {
                if ($registo->usuario->estacao->municipio->provincia->id == $id && $registo->usuario->estacao->tipo == $tipo) {
                    $total++;
                }
            } else {

                if ($registo->usuario->estacao->municipio->provincia->id == $id) {
                    $total++;
                }
            }
        }
    }

    return $total;
}

 



function get_data_by_municipio($tipo, $municipio_id, $start = null, $end = null)
{

    switch ($tipo) {
        case 'recebido':
            $total = 0;

            if ($start && $end) {
                $registos = formulario::whereBetween('created_at', [$start, $end])->get();
            } else {
                $registos = formulario::all();
            }

            if ($registos) {

                foreach ($registos as $registo) {
                    if ($registo->estacao->municipio->id == $municipio_id) {
                        $total++;
                    }
                }
            }

            return $total;

            break;

        case 'expedido':
            $total = 0;

            if ($start && $end) {
                $registos = formulario::whereBetween('created_at', [$start, $end])->get();
            } else {
                $registos = formulario::all();
            }

            if ($registos) {

                foreach ($registos as $registo) {
                    if ($registo->usuario->estacao->municipio->id  == $municipio_id) {
                        $total++;
                    }
                }
            }

            return $total;
            break;
        default:
            # code...
            break;
         }
}







function getBy($tipo, $estacao_id, $start = null, $end = null)
{

    switch ($tipo) {
        case 'recebido':
            $total = 0;

            if ($start && $end) {
                $registos = formulario::whereBetween('created_at', [$start, $end])->get();
            } else {
                $registos = formulario::all();
            }

            if ($registos) {

                foreach ($registos as $registo) {
                    if ($registo->id_estacao_trabalho_recebe == $estacao_id) {
                        $total++;
                    }
                }
            }

            return $total;

            break;

        case 'expedido':
            $total = 0;

            if ($start && $end) {
                $registos = formulario::whereBetween('created_at', [$start, $end])->get();
            } else {
                $registos = formulario::all();
            }

            if ($registos) {

                foreach ($registos as $registo) {
                    if ($registo->usuario->estacao->id == $estacao_id) {
                        $total++;
                    }
                }
            }

            return $total;
            break;

            
        default:
            # code...
            break;
    }
}

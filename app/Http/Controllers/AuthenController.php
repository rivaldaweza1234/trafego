<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\usuarios;
use Illuminate\Support\Facades\Hash;

class AuthenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        if(Auth::check()){

            return redirect('dashboard');

        }
        
        return view('login');
    }

    public function checkUser(Request $request)
    {

        $user = usuarios::where('codigo', $request->codigo)->first();
        if($user && $user->estacao->estado != "Inactiva"){
            $auth = Auth::attempt([
                'codigo' => $request->codigo,
                'password' => $request->password
            ]);

            if($auth){
            
                return redirect('dashboard');
    
            }else{
    
                return redirect('login')->with('error', 'Erro ao logar. a estação a que pertence está actualmente INACTIVA!');
            }
        }else{ 
                    return redirect('login')->with('error', 'Erro ao logar. verifique as suas credencias!');
        }

        
    }

    public function logout()
    {
        Auth::logout();

        return redirect('login');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\estacao_trabalho;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\tipo_operacao;
use App\Models\tipo_trafego;
use App\Models\grau_precedencia;
use App\Models\grau_seguranca;
use App\Models\provincia;
use App\Models\comuna;
use App\Models\municipio;
use App\Models\tipo_ocorrencia;
use App\Models\formulario;
use App\Models\ocorrencia;
use App\usuarios;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use PDF;

class TrafegoController extends Controller
{

    protected $months = [
        "01" => "Janeiro",
        "02" => "Fevereiro",
        "03" => "Março",
        "04" => "Abril",
        "05" => "Maio",
        "06" => "Junho",
        "07" => "Julho",
        "08" => "Agosto",
        "09" => "Setembro",
        "10" => "Outubro",
        "11" => "Novembro",
        "12" => "Dezembro"
    ];

    protected $years = [
        "2020" => "2020",
        "2019" => "2019",
        "2018" => "2018"
    ];

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   //Repor index normal
        return view('dashboard')->withDa(formulario::all())
            ->withTotalAceitacao(formulario::all()->count())
            ->withTotalEscoamento(formulario::all()->count())
            ->withTotalOcorrencia(ocorrencia::count())
            ->withTotalUsuarios(usuarios::count());
    }


    public function Estacao()
    {

        return view('estatistica_estacao')
            ->withEstacao(estacao_trabalho::all())
            ->withMunicipio(municipio::all())
            ->withProvincia(provincia::all());
           
    }


    public function ocorrencia()
    {

        return view('estatistica_occur')->withTipoOcorrencias(tipo_ocorrencia::all())
            ->withOcorrencias(ocorrencia::all())
            ->withMonths($this->months)
            ->withYears($this->years);
    }

    public function trafego()
    {

        if (Auth::user()->nivel == 'Admin') {
            return view('estatistica_traffic')
                ->withRegistos(formulario::all())
                ->withProvincias(provincia::all())
                ->withMunicipios(municipio::where("provincia_id", Auth::user()->estacao->municipio->provincia_id)->get());
        } else {

            return view('estatistica_traffic')->withRegistos(formulario::all())
                ->withTrafego(tipo_trafego::all())
                ->withgrau(Grau_precedencia::all())
                ->withseguranca(grau_seguranca::all())
                ->withEstacoes(estacao_trabalho::all())
                ->withProvincias(provincia::all())
                ->withMunicipios(municipio::all())
                ->withMonths($this->months)
                ->withYears($this->years);
        }
    }
   
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('da')->withDa(formulario::all());
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
 
    public function print(Request $request)
    {
        //dd($request); 

        if ($request->type == "occur") {

            $start = date($request->data_inicio, time());
            $end = date($request->data_fim, time());

            if (!isset($request->tipo_ocorrencia_id)) {

                $ocorrencias = ocorrencia::whereBetween('created_at', [$start, $end])->get();
            } else {

                $ocorrencias = ocorrencia::where('id_tipo_ocorrencia', $request->tipo_ocorrencia_id)->whereBetween('created_at', [$start, $end])->get();
            }
            //dd($ocorrencias);

            $pdf = PDF::loadView('pdf_estatistica', compact('ocorrencias'));
        } else{
 
            Carbon::setLocale('pt');
            $timestemp = Carbon::now();
            $year = Carbon::createFromFormat('Y-m-d H:i:s', $timestemp)->year;
            $month = Carbon::createFromFormat('Y-m-d H:i:s', $timestemp)->monthName;
            

            if ($request->comuna_id) {                
                $estacao = estacao_trabalho::where('id', $request->comuna_id)->first();
                
              
                $pdf = PDF::loadView('pdf_estatistica', compact('estacao', 'month', 'year'));  
                return $pdf->setPaper('A4', 'landscape')->stream('Registos.pdf');
            }

            elseif ($request->municipio_id) {                
                $municipio = municipio::where('id', $request->municipio_id)->first();
                //dd($estacao);

                $pdf = PDF::loadView('pdf_estatistica', compact('municipio', 'month', 'year'));  
                return $pdf->setPaper('A4', 'landscape')->stream('Registos.pdf');
            }
            if($request->estacao_id) {                
                $est = estacao_trabalho::where('id', $request->estacao_id)->first();
                
              
                $pdf = PDF::loadView('pdf_estatistica', compact('est', 'month', 'year'));  
                return $pdf->setPaper('A4', 'landscape')->stream('Registos.pdf');
            }
            
            $provincia_id = ($request->all == "on") ? $request->provincia_origem_multiple : $request->provincia_origem ;
           //dd($request->provincia_origem);
            if ($request->all == "on" && !$request->provincia_origem_multiple) {
                
                $provincias = provincia::all();
                $pdf = PDF::loadView('pdf_estatistica', compact('provincias', 'month', 'year'));  

            } else{
                $provincias = provincia::whereIn('id', $provincia_id)->get();
                $pdf = PDF::loadView('pdf_estatistica', compact('provincias', 'month', 'year'));
            }
            
        }

        return $pdf->setPaper('A4', 'landscape')->stream('Registos.pdf');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

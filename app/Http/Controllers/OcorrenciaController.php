<?php

namespace App\Http\Controllers;

 
use Illuminate\Http\Request;
use App\usuarios;
use App\Models\provincia;
use App\Models\grau_precedencia;
use App\Models\formulario;
use App\Models\tipo_operacao;
use App\Models\tipo_trafego;
use App\Models\tipo_ocorrencia;
use App\Models\ocorrencia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Khill\Lavacharts\Lavacharts;

class OcorrenciaController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('ver_ocorrencia')->withOcorrencias(ocorrencia::all());
    }
    
    public function create()
    {
            return view('ocorrencia')
                        ->withocorrencia(tipo_ocorrencia::all())
                        ->withocorrencia(tipo_ocorrencia::all());
    }

    public function store(Request $request)
    {
        //dd(Auth::user()->id);

        $ocorrencia = new ocorrencia();
 

        $ocorrencia->mensagem = $request->mensagem;
        $ocorrencia->id_usuario = Auth::user()->id;
      
        $ocorrencia->id_tipo_ocorrencia = $request->tipo_ocorrencia_id;

        

            if($ocorrencia->save()){

                return redirect('ocorrencia')->with('success', 'cadastrado com sucesso!');
    
            }else{
    
                return redirect('ocorrencia')->with('error', 'Não foi possível cadastrar!');
    
            }

        
        
    }
    public function edit($id)
    { 
        
        return view('ocorrencia_edit')
        ->withOcorrencia(ocorrencia::find($id))
        ->withTipoOcorrencia(tipo_ocorrencia::all());


    }

    
    public function update(Request $request, $id)
    { 
        
         //dd($request);

         $ocorrencia = ocorrencia::find($id);

         $ocorrencia->mensagem = $request->mensagem; 
         
      
         $ocorrencia->id_tipo_ocorrencia = $request->tipo_ocorrencia_id;
   
         if($ocorrencia->save()){
 
             return redirect('ocorrencia')->with('success', 'Dados atualizados!');
 
         }else{
 
             return redirect('ocorrencia')->with('error', 'Não foi possível atualizar os dados do utilizador!');
 
         }

    }

       /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    { 
        
        $oc_action = ocorrencia::find($id);
        $oc_action->delete();

        return redirect('ocorrencia')->with('success', 'Ocorrencia eliminada');

    }
    
    public function show($id)
    {
             

    }



}





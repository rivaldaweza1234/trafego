<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\estacao_trabalho;
use App\Models\municipio;
use App\Models\comuna;
use App\usuarios;
use App\Models\provincia;
use Illuminate\Support\Facades\Auth;



class EstacoesController extends Controller
{

    protected $months = [
        "01" => "Janeiro",
        "02" => "Fevereiro",
        "03" => "Março",
        "04" => "Abril",
        "05" => "Maio",
        "06" => "Junho",
        "07" => "Julho",
        "08" => "Agosto",
        "09" => "Setembro",
        "10" => "Outubro",
        "11" => "Novembro",
        "12" => "Dezembro"
    ];

    protected $years = [
        "2020" => "2020",
        "2019" => "2019",
        "2018" => "2018"
    ];


    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('estacoes.index')->withEstacoes(estacao_trabalho::all());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::user()->nivel == 'Admin') {
            return view('estacoes.create')->withProvincias(provincia::all())->withMunicipios(municipio::where("provincia_id", Auth::user()->estacao->municipio->provincia_id)->get());
        }
        return view('estacoes.create')->withProvincias(provincia::all())->withMunicipios(municipio::all());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $estacao = new estacao_trabalho();

        $estacao->nome = $request->nome;
        $estacao->tipo = $request->tipo;
        $estacao->municipio_id = $request->municipio_id;

        $__Verifica = estacao_trabalho::where('municipio_id', $request->municipio_id)
            ->where('nome', $request->nome)
            ->where('tipo', $request->tipo)
            ->count();

        if ($__Verifica < 1) {


            if ($estacao->save()) {

                $comuna = new comuna();

                $comuna->nome = $request->nome;

                $comuna->estacao_id = $estacao->id;

                $comuna->save();

                return redirect('estacao')->with('success', 'Estação cadastrada!');
            } else {

                return redirect('estacao')->with('error', 'Não foi possível cadastrar a estação!');
            }
        } else {

            return redirect('estacao')->with('error', 'Já existe uma estação com os dados inseridos');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    public function toggleState(estacao_trabalho $estacao)
    {
        $estacao->estado = ($estacao->estado == "Activa") ? "Inactiva" : "Activa";
        if ($estacao->save()) {

            return redirect('estacao')->with('success', 'Dados atualizados!');
        } else {

            return redirect('estacao')->with('error', 'Não foi possível atualizar os dados da estação!');
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $Estacao = estacao_trabalho::find($id);
        return view('estacoes.edit')->withEstacao(estacao_trabalho::find($id))->withProvincias(provincia::all())->withMunicipios(municipio::where("provincia_id", $Estacao->municipio->provincia_id)->get());
    }

    public function MunicipiosByProvincia($provincia_id)
    {

        return response()->json(municipio::where("provincia_id", $provincia_id)->get());
    }

    public function ComunaByMunicipio($municipio_id)
    {

        return response()->json(estacao_trabalho::where("municipio_id", $municipio_id)->where("tipo", "Comunal")->get());
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, estacao_trabalho $estacao)
    {

        $estacao->nome = $request->nome;
        $estacao->tipo = $request->tipo;
        $estacao->municipio_id = $request->municipio_id;

        if ($estacao->save()) {

            return redirect('estacao')->with('success', 'Dados atualizados!');
        } else {

            return redirect('estacao')->with('error', 'Não foi possível atualizar os dados da estação!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $estacao = estacao_trabalho::find($id);

        $__envolved_on_user = usuarios::where('id_estacao_trabalho', $id)->count();
        //$__envolved_on_form = usuarios::where('id_usuario', $id)->count();

        if ($__envolved_on_user) {

            return redirect('estacao')->with('error', 'Não é possível eliminar a estação <strong>"' . $estacao->nome . '"</strong>, pois já existem usuários vinculados a mesma.');
        }

        $estacao->delete();
        return redirect('estacao')->with('success', 'Estação eliminada');
    }
}

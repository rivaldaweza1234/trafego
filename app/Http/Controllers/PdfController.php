<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\formulario;

use PDF;
class PdfController extends Controller
{
     
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    Public function geraPdf($id){
      
        
        $registo = formulario::find($id);
       
        $pdf = PDF::loadView('pdf', compact('registo'));

        return $pdf->setPaper('A4')->stream('Registos.pdf');

    }
    
      
    Public function geraPdf2($id){
      
        $escoamento = formulario::find($id);
       
        $pdf2 = PDF::loadView('pdf2', compact('escoamento'));

        return $pdf2->setPaper('a4')->stream('Escoamento.pdf');

    }


}

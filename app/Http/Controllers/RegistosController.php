<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\usuarios;
use App\Models\estacao_trabalho;

use App\Models\grau_seguranca;
use App\Models\grau_precedencia;
use App\Models\formulario;
use App\Models\tipo_operacao;
use App\Models\tipo_trafego;
use App\Models\tipo_ocorrencia;
use App\Models\ocorrencia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;


class RegistosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('ver_registos')->withRegisto(formulario::orderBy('id', 'desc')->get());
    }

    public function Recebidos($id){
        $Registo = formulario::find($id);
        $Registo->estado = "Recebido";

        $Registo->save();

        return response()->json(["success" => "done"]);

    }

    public function Invalidos($id){
        $Registo = formulario::find($id);
        $Registo->estado = "Inválido";

        $Registo->save();

        return response()->json(["success" => "done"]);

    }

    public function create()
    {
        return view('registo')
            ->withoperacao(tipo_Operacao::all())
            ->withTrafego(tipo_trafego::all())
            ->withgrau(Grau_precedencia::all())
            ->withseguranca(grau_seguranca::all())
            ->withEstacoes(estacao_trabalho::where('id', '!=', Auth::user()->estacao->id)->get());
    }

    public function store(Request $request)
    {



        $registo = new formulario();


        $registo->id_usuario = Auth::user()->id;

        $registo->origem = $request->origem;
        $registo->ano = $request->ano;
        $registo->serie = $request->serie;
        $registo->envia = $request->envia;
        $registo->recebe = $request->recebe;
        $registo->id_tipo_operacao = $request->tipo_operacao_id;
        $registo->id_tipo_trafego = $request->tipo_trafego_id;
        $registo->id_grau_precedencia = $request->grau_precedencia_id;

        $registo->id_grau_seguranca = $request->grau_seguranca_id;
        $registo->id_estacao_trabalho_recebe = $request->id_estacao_trabalho_recebe;


        if ($registo->save()) {

            return redirect('registo')->with('success', 'cadastrado com sucesso!');
        } else {

            return redirect('registo')->with('error', 'Não foi possível cadastrar!');
        }
    }

    public function show()
    {

        return view('ver_registos')->withRegistos(formulario::all());
    }
    public function download($id)
    {
    }


    public function edit($id)
    {

        return view('registo_edit')
            ->withRegisto(formulario::find($id))
            ->withTipoOperacao(tipo_operacao::all())
            ->withTipoTrafego(tipo_trafego::all())
            ->withGrauPrecedencia(grau_precedencia::all())
            ->withGrauSeguranca(grau_seguranca::all())
            ->withEstacoes(estacao_trabalho::all());
    }




    public function update(Request $request, $id)
    {

        //dd($request);

        $registo = formulario::find($id);
        $registo->id_tipo_operacao = $request->tipo_operacao_id;
        $registo->id_tipo_trafego = $request->tipo_trafego_id;
        $registo->id_grau_seguranca = $request->grau_seguranca_id;
        $registo->id_estacao_trabalho_recebe = $request->id_estacao_trabalho_recebe;
        $registo->envia = $request->envia;
        $registo->recebe = $request->recebe;
        $registo->origem = $request->origem;
        $registo->ano = $request->ano;
        $registo->serie = $request->serie;



        if ($registo->save()) {

            return redirect('registo')->with('success', 'Dados atualizados!');
        } else {

            return redirect('registo')->with('error', 'Não foi possível atualizar os dados!');
        }
    }


    public function destroy($id)
    {

        $ec_action = formulario::find($id);
        $ec_action->delete();

        return redirect('registo')->with('success', 'Registo eliminado');
    }
}

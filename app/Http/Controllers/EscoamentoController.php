<?php

namespace App\Http\Controllers;

 
use Illuminate\Http\Request;
use App\usuarios;
use App\Models\estacao_trabalho; 
use App\Models\ficheiro;
 
use App\Models\grau_seguranca;
use App\Models\grau_precedencia;
use App\Models\formulario;
use App\Models\tipo_operacao;
use App\Models\tipo_trafego;
use App\Models\tipo_ocorrencia;
use App\Models\ocorrencia;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class EscoamentoController extends Controller
{ 
    private $totalPage =2;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $Request)
    { 
        return view('ver_escoamentos')->withEscoamento(formulario::orderBy('id', 'desc')->get());
    }

    public function create()
    {
           
            return view('escoamento') 
                        ->withTrafego(tipo_trafego::all())
                        ->withgrau(Grau_precedencia::all())
                        ->withseguranca(grau_seguranca::all())
                        ->withEstacoes(estacao_trabalho::where('id', '!=', Auth::user()->estacao->id)->where('estado', '!=', 'Inactiva')->get() );
                      

    }

    public function grauById(grau_precedencia $grau)
    {
        return $grau;
    }
    public function grauSegurancaById(grau_seguranca $grauSe)
    {
        return $grauSe;
    }

    public function store(Request $request)
    {
        //dd($request->id_estacao_trabalho_recebe);

        foreach($request->id_estacao_trabalho_recebe as $estacao_id){

            $escoamento = new formulario();

            $escoamento->id_usuario = Auth::user()->id;
    
            $escoamento->origem = $request->origem; 
         
            $escoamento->numero_serie = $request->numero_serie; 
            $escoamento->mes = $request->mes; 
            $escoamento->mensagem = $request->mensagem; 
            $escoamento->envia = $request->envia; 
            $escoamento->recebe = $request->recebe; 
            $escoamento->anexo = $request->anexo->store('doc');
          
            $escoamento->id_tipo_trafego = $request->tipo_trafego_id;
            $escoamento->id_grau_precedencia = $request->grau_precedencia_id;  
    
            $escoamento->id_usuario = Auth::user()->id;
            
            $escoamento->id_grau_seguranca = $request->grau_seguranca_id; 
     
            $escoamento->id_estacao_trabalho_recebe = $estacao_id;

            $escoamento->save();
            
        }
        
        return redirect('escoamento')->with('success', 'cadastrado com sucesso!');
        
    }

    public function show()
    {
       
        return view('ver_escoamentos')->withEscoamento(formulario::all());
      
   
    }
    
    public function edit($id)
    { 
        
        return view('escoamento_edit')
        ->withEscoamento(formulario::find($id)) 
        ->withTipoTrafego(tipo_trafego::all())
        ->withGrauPrecedencia(grau_precedencia::all())
        ->withGrauSeguranca(grau_seguranca::all())  
        ->withEstacoes(estacao_trabalho::all());


    }

    
    public function update(Request $request, $id)
    { 
        
         //dd($request);

         $escoamento = formulario::find($id); 
         $escoamento->id_tipo_trafego = $request->tipo_trafego_id; 
         $escoamento->id_grau_seguranca = $request->grau_seguranca_id; 
         $escoamento->id_estacao_trabalho_recebe = $request->id_estacao_trabalho_recebe;  
         $escoamento->envia = $request->envia; 
         $escoamento->recebe = $request->recebe; 
         $escoamento->origem = $request->origem; 
         $escoamento->mensagem = $request->mensagem; 
         $escoamento->numero_serie = $request->numero_serie; 
        
         
       
   
         if($escoamento->save()){
 
             return redirect('escoamento')->with('success', 'Dados atualizados!');
 
         }else{
 
             return redirect('escoamento')->with('error', 'Não foi possível atualizar os dados!');
 
         }

    }

       /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    
    public function destroy($id)
    { 
        
        $ec_action = formulario::find($id);
        $ec_action->delete();

        return redirect('escoamento')->with('success', 'Escoamento eliminada');

    }


    public function historic()
    { 
        
        $historics = auth()->user()
        ->hostorics()
        ->with(['userSender'])
        ->paginate($this->total);
         
        return redirect('dashboard', compact('historics'));

    }



}



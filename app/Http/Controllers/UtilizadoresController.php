<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\usuarios;
use App\Models\formulario;
use App\Models\estacao_trabalho;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UtilizadoresController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->nivel == "Usuário"){

            return redirect('dashboard');

        }

        return view('usuarios.index')->withUtilizadores(usuarios::where('nivel', '!=', 'SuperUsuário')->get());
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('usuarios.create')->withEstacoes(estacao_trabalho::where('estado','Activa')->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $usuario = new usuarios();
        $last_code = usuarios::max('codigo');

        $usuario->codigo = $last_code + 1;
        $usuario->nome = $request->nome;
        $usuario->password = Hash::make($request->password);
        $usuario->nivel = $request->nivel;
        $usuario->id_estacao_trabalho = $request->id_estacao_trabalho;

        $__Verifica = usuarios::where('codigo', $request->codigo)->count();

        if($__Verifica < 1){

            if($usuario->save()){

                return redirect('utilizador')->with('success', 'Utilizador cadastrado!');
    
            }else{
    
                return redirect('utilizador')->with('error', 'Não foi possível cadastrar utilizador!');
    
            }

        }else{

            return redirect('utilizador')->with('error', 'Já existe um utilizador com o código: '.$request->codigo);

        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $usuario = usuarios::find($id);

        return view('usuarios.edit')
                    ->withUtilizador($usuario)
                    ->withEstacoes(estacao_trabalho::all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //dd($request);
        $usuario = usuarios::find($id);
        $usuario->nome  = $request->nome;

        if(isset($request->password) && $request->password != ""){

            $usuario->password  = Hash::make($request->password);

        }

        $usuario->nivel  = $request->nivel;
        $usuario->id_estacao_trabalho  = $request->id_estacao_trabalho;

        if($usuario->save()){

            return redirect('utilizador')->with('success', 'Dados atualizados!');

        }else{

            return redirect('utilizador')->with('error', 'Não foi possível atualizar os dados do utilizador!');

        }
    }

    public function updatePwd(Request $request)
    {
        $rules = array(
            'password' => 'required',
            'new_password' => 'required',
            'new_password_confirm' => 'required|same:new_password',
        );

        $messages = [
            'required' => 'O campo :attribute deve ser preenchido.',
            'attributes' => [
                'new_password' => 'Nova Senha',
            ],
        ];

        $error = Validator::make($request->all(), $rules, $messages);

        if($error->fails()){

            return response()->json(['errors'=> $error->errors()->all()]);

        }elseif(!Hash::check($request->password, Auth::user()->password)){

            return response()->json(['error'=> 'Senha actual incorrecta']);

        }else{

            $usuario = usuarios::find(Auth::user()->id);
            $usuario->password = Hash::make($request->new_password);
            $usuario->save();
            
        }

        return response()->json(['success'=>'A sua senha foi alterada com sucesso. A sair...']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
        public function destroy($id)
    {
        $usuario = usuarios::find($id);
        
        $user_action = formulario::where('id_usuario', $id)->count();


        
        if(Auth::user()->id == $id){
            
            $usuario->delete();
            Auth::logout();
            return redirect('login');

        }elseif($user_action){

            return redirect('utilizador')->with('error', 'Não é possível eliminar este utilizador, pois já tem ações gravadas no sistema');

        }

        $usuario->delete();
        return redirect('utilizador')->with('success', 'Utilizador eliminado');

    }

      

     
}

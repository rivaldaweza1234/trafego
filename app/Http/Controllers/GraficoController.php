<?php

namespace App\Http\Controllers;

 
use Illuminate\Http\Request;
use App\usuarios;
use App\Models\provincia;
use App\Models\grau_precedencia;
use App\Models\formulario;
use App\Models\tipo_operacao;
use App\Models\tipo_trafego;
use App\Models\tipo_ocorrencia;
use App\Models\ocorrencia;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Khill\Lavacharts\Lavacharts;

class GraficoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view ('grafico')
         ->withOcorrencia(ocorrencia::all());
        
        $ocorrencia = ocorrencia::find($id);

        $notas = array(0, 0, 0, 0, 0);
        foreach($ocorrencia -> ratings as $rating)
              
        {
                $notas [ $rating -> rating -1] +1 ;
        }

        $lava = new Lavacharts();
        $graficoNotas = $lava -> DataTable();
        $graficoNotas->addstringColumn('mensagem');

        for($i -1; $i <-count($notas); $i++ )
              
                {
                    $graficoNotas-> addRow(['No' . $i, $notas[$i - 1]]);      
                }

                $lava ->BarChart('mensagem', $graficoNotas);
    }

}
